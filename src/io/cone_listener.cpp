//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "cone_listener.hpp"
namespace estimation{
d4_comm_pkg::ConeArray Cone_Listener::PCL_to_cone_array ( const sensor_msgs::PointCloud2::ConstPtr &cones_in )
{
  d4_comm_pkg::ConeArray retval;
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_cones ( new pcl::PointCloud<pcl::PointXYZ> );
  pcl::fromROSMsg ( *cones_in,*in_cones );
  retval.cones.reserve(in_cones->points.size());
  for ( pcl::PointXYZ &p: in_cones->points )
    {
      d4_comm_pkg::Cone cone;
      cone.x   = 	p.x;
      cone.y  = 	p.y;
      cone.header.stamp = cones_in->header.stamp;
      retval.cones.push_back ( cone );
    }
  retval.header = cones_in->header;
  return retval;
}//endfunc
//****************************************************************

void Cone_Listener::CBPCL ( const sensor_msgs::PointCloud2_< allocator< void > >::ConstPtr& cones_in )
{
  auto data = PCL_to_cone_array ( cones_in );
  AddMissingInformation(data);
  cb_data ( data,Cone_Listener::params_.id );
}//endfunc
//****************************************************************

void Cone_Listener::CBCones ( const d4_comm_pkg::ConeArray_< allocator< void > >::ConstPtr& cones_in )
{
  auto c_array = cones_in.operator*();
  AddMissingInformation(c_array);
  cb_data ( c_array,params_.id );
}//endfunc
//****************************************************************

void Cone_Listener::InitBaseLinkTransformation(d4_comm_pkg::ConeArray &cones)
{
  ROS_INFO_STREAM("Source Frame for sensor : " <<  params_.name << " is " << cones.header.frame_id);
  ROS_INFO_STREAM("base link " << params_.base_link);
  tfListener_ = std::make_unique<tf2_ros::TransformListener>(tfbuffer_);
  try
  {
    transform_stamped_ = tfbuffer_.lookupTransform(params_.base_link, cones.header.frame_id,ros::Time(0),ros::Duration(1.0));
  }
  catch (tf2::TransformException &ex)
  {
    ROS_ERROR_STREAM(ex.what());
    ROS_BREAK();
  }
  base_link_initialized = true;
}//endfunc
//****************************************************************

void Cone_Listener::AddMissingInformation(d4_comm_pkg::ConeArray &cones)
{
  if ( params_.covar_given == false )
    {
      for ( d4_comm_pkg::Cone &c : cones.cones )
        {
          c.covariance.reserve(2);
          c.covariance = {params_.covar_r, params_.covar_b};
        }
    }
  if(cones.header.frame_id != params_.base_link)
  {
    ROS_INFO_STREAM_ONCE(" Frame for Sensor " << params_.name << " is " << cones.header.frame_id);
    if(base_link_initialized == false)
    {
      InitBaseLinkTransformation(cones);
    }
    TransformToBaseLink(cones);
  }
}//endfunc
//****************************************************************

 void Cone_Listener::TransformToBaseLink(d4_comm_pkg::ConeArray &cones)
 {
   for (auto &c : cones.cones)
   {
     geometry_msgs::Point source;
     geometry_msgs::Point target;
     source.x = c.x;
     source.y = c.y;
     source.z = 0;
     tf2::doTransform(source,target,transform_stamped_);
     c.x = target.x;
     c.y = target.y;
    }
 }//endfunc
//****************************************************************

Cone_Listener::Cone_Listener ( ESTIMATION::Sensor_Params& params, std::function< void ( d4_comm_pkg::ConeArray,int ) > callback )
{
  cb_data = callback;
  params_ = params;
  ROS_INFO_STREAM("Base link for sensor : " <<  params_.name << " is " << params_.base_link);
  switch ( params.plattform )
    {
    case ESTIMATION::PLATTFORM::D4:
    case ESTIMATION::PLATTFORM::MARINA:
    {
      sub_cone_ = params.nh_->subscribe ( params.topic,1,&Cone_Listener::CBCones,this );
    }
    break;
    case ESTIMATION::PLATTFORM::SIM:
    {
      sub_cone_ = params.nh_->subscribe ( params.topic,1,&Cone_Listener::CBPCL,this );
    }
    break;
    default:
      ROS_ERROR_STREAM("No plattform provided for sensor " << params.name);
      ROS_BREAK();
    }
}//endfunc
//****************************************************************
}//End namepsace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
