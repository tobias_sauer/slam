//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "visualizer.hpp"

Visualization::Visualization ( ros::NodeHandle *nh )
{

  pub_map_ = nh->advertise<visualization_msgs::Marker> ( "/viz/map",1000 );
  pub_cone_ = nh->advertise<visualization_msgs::Marker> ( "/viz/cones",1000 );
  pub_relative_ = nh->advertise<visualization_msgs::Marker> ( "/viz/relative",1000 );
  pub_path_ = nh->advertise<visualization_msgs::Marker> ( "/viz/path",1000 );
}


void Visualization::PubCone ( float x, float y )
{
  visualization_msgs::Marker line_list;
  line_list.lifetime = ros::Duration();

  line_list.header.frame_id = "/map";
  line_list.header.seq=seq_cones_;
  line_list.header.stamp = ros::Time::now();

  line_list.type = visualization_msgs::Marker::POINTS;
  line_list.action =  visualization_msgs::Marker::ADD;

  line_list.color.g = 1.0f;
  line_list.color.a = 1;
  line_list.scale.x = 1;
  line_list.scale.y = 1;
  line_list.scale.z = 1;

  geometry_msgs::Point p;

  p.x = x;
  p.y = y;
  line_list.points.push_back ( p );

  line_list.id = seq_cones_;
  seq_cones_++;
  pub_cone_.publish<visualization_msgs::Marker> ( line_list );

}

void Visualization::Relative ( vector<pair<float,float>> &cones )
{
  visualization_msgs::Marker line_list;
  line_list.lifetime = ros::Duration();

  line_list.header.frame_id = "/map";
  line_list.header.seq=seq_;
  line_list.header.stamp = ros::Time::now();

  line_list.type = visualization_msgs::Marker::POINTS;
  line_list.action =  visualization_msgs::Marker::ADD;

  line_list.color.b = 1.0f;
  line_list.color.a = 0.3f;
  line_list.scale.x = 1;
  line_list.scale.y = 1;
  line_list.scale.z = 1;



  for ( pair<float,float> pc : cones )
    {
      geometry_msgs::Point p;
      p.x = pc.first;
      p.y = pc.second;
      line_list.points.push_back ( p );
    }

  line_list.id = seq_;
  seq_++;

  pub_relative_.publish<visualization_msgs::Marker> ( line_list );

}

void Visualization::PubMap ( map< uint64_t, pair< float, float > > &input )
{
  visualization_msgs::Marker line_list;
  line_list.lifetime = ros::Duration();

  line_list.header.frame_id = "/map";
  line_list.header.seq=seq_;
  line_list.header.stamp = ros::Time::now();

  line_list.type = visualization_msgs::Marker::POINTS;
  line_list.action =  visualization_msgs::Marker::ADD;

  line_list.color.r = 1.0f;
  line_list.color.a = 1;
  line_list.scale.x = 1;
  line_list.scale.y = 1;
  line_list.scale.z = 1;

  geometry_msgs::Point p;
  uint32_t id  = 0;
  for ( pair<size_t,pair<float,float>> mp : input )
    {
      geometry_msgs::Point p;
      p.z=0;
      p.x = mp.second.first;
      p.y = mp.second.second;
      line_list.points.push_back ( p );
    }
  line_list.id = seq_;
  seq_++;
  pub_map_.publish<visualization_msgs::Marker> ( line_list );
}


void Visualization::VizPath ( float x, float y,bool derived )
{
  visualization_msgs::Marker line_list;
  line_list.lifetime = ros::Duration();

  line_list.header.frame_id = "/map";
  line_list.header.seq=seq_cones_;
  line_list.header.stamp = ros::Time::now();

  line_list.type = visualization_msgs::Marker::POINTS;
  line_list.action =  visualization_msgs::Marker::ADD;

  if ( derived == false )
    {
      line_list.color.b = 1.0f;
    }
  else
    {
      line_list.color.r = 1.0f;
    }

  line_list.color.a = 1;
  line_list.scale.x = 0.05;
  line_list.scale.y = 0.05;
  line_list.scale.z = 0.05;

  geometry_msgs::Point p;

  p.x = x;
  p.y = y;
  line_list.points.push_back ( p );

  line_list.id = seq_cones_;
  seq_cones_++;
  pub_cone_.publish<visualization_msgs::Marker> ( line_list );
}


/*
void Visualization::Draw_Track(const Track &t)
{
	seq_ = 0;
	visualization_msgs::Marker line_list;
	line_list.lifetime = ros::Duration();

	line_list.header.frame_id = local_params_.frame;
	line_list.header.seq=0;
	line_list.header.stamp = ros::Time::now();

	line_list.type = visualization_msgs::Marker::LINE_STRIP;
	line_list.action =  visualization_msgs::Marker::ADD;

	line_list.color.r = 0;
	line_list.color.g = (float)0;
	line_list.color.b =1.0f;
	line_list.color.a = 1;
	line_list.scale.x = 0.2f;
	line_list.scale.y = 0.2f;
	line_list.scale.z = 0.2f;

	for(size_t i=0; i < t.left_boundarie_.size();i++)
	{
		geometry_msgs::Point p_a;
		p_a.z=0;
		p_a.x = t.left_boundarie_[i].first;
		p_a.y = t.left_boundarie_[i].second;
		line_list.points.push_back(p_a);
	}

	line_list.id = 0;
	seq_++;
	pub_line_.publish<visualization_msgs::Marker>(line_list);
	line_list.points.clear();


	line_list.lifetime = ros::Duration();

	line_list.header.frame_id = local_params_.frame;
	line_list.header.seq=seq_;
	line_list.header.stamp = ros::Time::now();

	line_list.type = visualization_msgs::Marker::LINE_STRIP;
	line_list.action =  visualization_msgs::Marker::ADD;

	line_list.color.r = 0;
	line_list.color.g = (float)1;
	line_list.color.b =0;
	line_list.color.a = 1;
	line_list.scale.x = 0.2f;
	line_list.scale.y = 0.2f;
	line_list.scale.z = 0.2f;

	for(size_t i=0; i < t.mid_boundarie_.size();i++)
	{
		geometry_msgs::Point p_a;
		p_a.z=0;
		p_a.x = t.mid_boundarie_[i].first;
		p_a.y = t.mid_boundarie_[i].second;
		line_list.points.push_back(p_a);
	}

	line_list.id = seq_;
	seq_++;
	pub_line_.publish<visualization_msgs::Marker>(line_list);
	line_list.points.clear();



	line_list.lifetime = ros::Duration();

	line_list.header.frame_id = local_params_.frame;
	line_list.header.seq=seq_;
	line_list.header.stamp = ros::Time::now();

	line_list.type = visualization_msgs::Marker::LINE_STRIP;
	line_list.action =  visualization_msgs::Marker::ADD;

	line_list.color.r = (float)1;
	line_list.color.g = (float)0;
	line_list.color.b = 1.0f;
	line_list.color.a = 1;
	line_list.scale.x = 0.2f;
	line_list.scale.y = 0.2f;
	line_list.scale.z = 0.2f;

	for(size_t i=0; i < t.right_boundarie_.size();i++)
	{
		geometry_msgs::Point p_a;
		p_a.z=0;
		p_a.x = t.right_boundarie_[i].first;
		p_a.y = t.right_boundarie_[i].second;
		line_list.points.push_back(p_a);
	}



	line_list.id = seq_;
	seq_++;
	pub_line_.publish<visualization_msgs::Marker>(line_list);


}


void Visualization::Visualize_Line(vector< pair< float, float > > line_in)
{
	visualization_msgs::Marker line_list;
	line_list.lifetime = ros::Duration();

	line_list.header.frame_id = local_params_.frame;
	line_list.header.seq=seq_;
	line_list.header.stamp = ros::Time::now();

	line_list.type = visualization_msgs::Marker::LINE_STRIP;
	line_list.action =  visualization_msgs::Marker::ADD;

	line_list.color.b = 1.0f;
	line_list.color.a = 1;
	line_list.scale.x = 0.2f;
	line_list.scale.y = 0.2f;
	line_list.scale.z = 0.2f;

	geometry_msgs::Point p_a;
	p_a.z=0;
	p_a.x = line_in[0].first;
	p_a.y = line_in[0].second;

		geometry_msgs::Point p_b;
	p_b.z=0;
	p_b.x = line_in[1].first;
	p_b.y = line_in[1].second;
	line_list.points.push_back(p_a);
	line_list.points.push_back(p_b);
	line_list.id = seq_;
	seq_++;
	//pub_line_.publish<visualization_msgs::Marker>(line_list);
}


void Visualization::Visualize_Delauny(delaunator::Delaunator& delauny)
{
	visualization_msgs::Marker line_list;
	 line_list.lifetime = ros::Duration();

	line_list.header.frame_id = local_params_.frame;
	seq_=0;
	line_list.header.seq=seq_;
	line_list.header.stamp = ros::Time::now();

	line_list.type = visualization_msgs::Marker::LINE_STRIP;
	line_list.action =  visualization_msgs::Marker::ADD;

	line_list.color.g = 1.0f;
	line_list.color.a = 1;
	line_list.scale.x = 0.1;
	line_list.scale.y = 0.1;
	line_list.scale.z = 0.1;
	ROS_INFO_STREAM("No of Triangles: " << delauny.triangles.size());
	for(size_t var=0; var < delauny.triangles.size(); var+=3)
	{
		geometry_msgs::Point p;
		p.z=0;

		p.x = delauny.coords[2 * delauny.triangles[var]];        //tx0
		p.y = delauny.coords[2 * delauny.triangles[var] + 1];    //ty0
		line_list.points.push_back(p);

		p.x =delauny.coords[2 * delauny.triangles[var + 1]];    //tx1
		p.y =delauny.coords[2 * delauny.triangles[var + 1] + 1];//ty1
		line_list.points.push_back(p);


		p.x =delauny.coords[2 * delauny.triangles[var + 2]];    //tx2
		p.y = delauny.coords[2 * delauny.triangles[var + 2] + 1]; //ty2
		line_list.points.push_back(p);

		p.x = delauny.coords[2 * delauny.triangles[var]];        //tx0
		p.y = delauny.coords[2 * delauny.triangles[var] + 1];    //ty0
		line_list.points.push_back(p);

		pub_.publish<visualization_msgs::Marker>(line_list);

		line_list.points.clear();
		line_list.header.seq++;
		line_list.id = line_list.header.seq;
		line_list.header.stamp = ros::Time::now();
		seq_++;
	}
}

void Visualization::Visualize_Point(float x, float y)
{
	visualization_msgs::Marker line_list;
	line_list.lifetime = ros::Duration();

	line_list.header.frame_id = local_params_.frame;
	line_list.header.seq=0;
	line_list.header.stamp = ros::Time::now();

	line_list.type = visualization_msgs::Marker::POINTS;
	line_list.action =  visualization_msgs::Marker::ADD;

	line_list.color.r = 1.0f;
	line_list.color.a = 1;
	line_list.scale.x = 1;
	line_list.scale.y = 1;
	line_list.scale.z = 1;

	geometry_msgs::Point p;
	p.z=0;
	p.x = x;
	p.y = y;
	line_list.points.push_back(p);
	line_list.id = 0;
	seq_++;
	pub_point_.publish<visualization_msgs::Marker>(line_list);
	ros::Duration(0.5).sleep();

}

*/

// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
