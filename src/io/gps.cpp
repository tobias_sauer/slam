#include "gps.hpp"

namespace estimation
{
  GPS::GPS(const ESTIMATION::GPSParams &params, std::function<void(gtsam::Pose2, ros::Time, Eigen::Matrix3f)> callback)
  {
    params_ = params;
    cb_data = callback;
    switch (params_.plattform)
    {
    case ESTIMATION::PLATTFORM::D4:
      break;
    case ESTIMATION::PLATTFORM::MARINA:
      break;
    case ESTIMATION::PLATTFORM::SIM:
      sub_gps_ = params_.nh_->subscribe<fssim_common::State>(params_.topic, 1, &GPS::CallbackSim, this);
      break;

    default:
      ROS_FATAL("gps no plattform selected");
      ROS_BREAK();
      break;
    }
  }

  void GPS::CallbackSim(const fssim_common::State_<std::allocator<void>>::ConstPtr &speed_in)
  {
    if( (speed_in->header.stamp - ros::Duration(1.0/(double)params_.frequency)) > last_sampe_time_)
    {
      ros::Duration(0.05).sleep(); //dilehy
      last_sampe_time_ = ros::Time::now();
      std::default_random_engine generator;
      std::normal_distribution<double> noise_x(0, params_.cov_x);
      std::normal_distribution<double> noise_y(0, params_.cov_y);
      std::normal_distribution<double> noise_th(0, params_.cov_theta);
      auto x = speed_in->x + noise_x(generator);
      auto y = speed_in->y + noise_y(generator);
      auto th = speed_in->yaw + noise_th(generator);
      Eigen::Matrix3f noise;
      noise(0, 0) = params_.cov_x;
      noise(1, 1) = params_.cov_y;
      noise(2, 2) = params_.cov_theta;
      cb_data(gtsam::Pose2(x,y,th),speed_in->header.stamp,noise);
    }
  }

}//end namespace