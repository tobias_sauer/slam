//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "pos_publisher.hpp"

namespace estimation{
using namespace std;
using namespace ESTIMATION;
Pos_Pub::~Pos_Pub()
{

}//endfunc
//****************************************************************


Pos_Pub::Pos_Pub ( ESTIMATION::Pub_Params &params )
{
  params_ = params;
  ros::NodeHandle *nh = params.nh_;
  pub_rtse_pose = nh->advertise<d4_comm_pkg::PoseVel> ( params.topic_position,1,true );
  pos_visualize = nh->advertise<geometry_msgs::PoseStamped> ( params.topic_viz_pos_map,1,true );
  pub_map = nh->advertise<d4_comm_pkg::RawMap> ( params.topic_map,1,true );
  viz_map = nh->advertise<visualization_msgs::Marker> ( params.topic_viz_finalised_map,1,true );
  seq = 0;

}//endfunc
//****************************************************************

void Pos_Pub::Publish ( d4_comm_pkg::PoseVel &out )
{
  out.header.stamp = ros::Time::now();
  out.header.seq = seq_pos_++;
  out.header.frame_id = "/base_link";
  pub_rtse_pose.publish<d4_comm_pkg::PoseVel> ( out );
  geometry_msgs::PoseStamped out_pose;

  out_pose.pose.position.x = out.Pose.pos_x;
  out_pose.pose.position.y = out.Pose.pos_y;
  out_pose.pose.orientation.w = cos ( out.Pose.pos_theta/2 );
  out_pose.pose.orientation.z = sin ( out.Pose.pos_theta/2 );
  auto frame_id = "map";
  out_pose.header.frame_id = frame_id;
  out_pose.header.stamp = ros::Time::now();
  out_pose.header.seq = seq;
  seq++;
  pos_visualize.publish<geometry_msgs::PoseStamped> ( out_pose );
}//endfunc
//****************************************************************

void Pos_Pub::PublishMap (const std::map< size_t, Measurement>& out,bool finalised )
{
  d4_comm_pkg::Cone cony;
  d4_comm_pkg::RawMap map;
  for ( std::pair<size_t,Measurement> elem : out )
    {
      d4_comm_pkg::Cone cony;
      cony.x = elem.second.x_;
      cony.y = elem.second.y_;
      map.cones.push_back ( cony );
    }
  map.finalised = finalised;
  map.header.stamp = ros::Time::now();
  map.header.seq = 0;
  map.header.frame_id = params_.frame_map;
  pub_map.publish<d4_comm_pkg::RawMap> ( map );

  visualization_msgs::Marker line_list;
  line_list.lifetime = ros::Duration();

  line_list.header.frame_id = params_.frame_map;
  line_list.header.seq=0;
  line_list.header.stamp = ros::Time::now();

  line_list.type = visualization_msgs::Marker::POINTS;
  line_list.action =  visualization_msgs::Marker::ADD;

  line_list.color.r = 1.0f;
  line_list.color.a = 1;
  line_list.scale.x = 0.3;
  line_list.scale.y = 0.3;
  line_list.scale.z = 0.3;

  geometry_msgs::Point p;
  uint32_t id  = 0;
  for ( std::pair<size_t,Measurement>  mp : out )
    {
      geometry_msgs::Point p;
      p.z=0;
      p.x = mp.second.x_;
      //ROS_INFO_STREAM(" x " <<  p.x);
      p.y = mp.second.y_;
      //ROS_INFO_STREAM(" y " <<  p.y);
      line_list.points.push_back ( p );
    }
  line_list.id = 0;
  ROS_INFO_STREAM(params_.topic_viz_finalised_map);
  
  viz_map.publish<visualization_msgs::Marker> ( line_list );
}//endfunc
//****************************************************************

}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
