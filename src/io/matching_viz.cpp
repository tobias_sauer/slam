#include "matching_viz.hpp"

namespace estimation
{

  MatchingViz::MatchingViz(ros::NodeHandle *nh)
  {
    pub_viz_ = nh->advertise<visualization_msgs::Marker>("/estimation/slam/viz_matching",100);
    std::thread t1(&MatchingViz::VizThread,this);

    t1.detach();
  }
  void MatchingViz::VizualizeMatching(std::shared_ptr<Node> node, const std::map<uint64_t, Measurement> &map)
  {
    queue.enqueue({node,map});
  }

  void MatchingViz::VizThread()
  {
    for(;;)
    {
      std::pair<std::shared_ptr<Node>, std::map<uint64_t, Measurement>> item;
      queue.wait_dequeue(item);
      auto n = item.first;
      std::map<uint64_t, Measurement> map = item.second;
      auto m = n->GetMeasuremnts();
      for(const auto cone : m  )
      {
        auto result = map.find(cone.key_meas_);
        if(cone.key_meas_ != MAGIC::NUMBER && result != std::end(map))
          {
            visualization_msgs::Marker marker;
            marker.header.frame_id = "map";
            marker.header.stamp = ros::Time().now();
            marker.ns = "my_namespace";
            marker.header.seq = seq_;
            marker.id = seq_++;
            marker.type = visualization_msgs::Marker::ARROW;
            marker.action = visualization_msgs::Marker::ADD;
            marker.lifetime = ros::Duration(0.2);
            auto map_target = map.at(cone.key_meas_);
            geometry_msgs::Point map_point;
            map_point.x = map_target.x_;
            map_point.y = map_target.y_;
            marker.points.push_back(map_point);

            const auto d_theta = n->pose_.pose_.theta();
            const auto drehung_x = cone.x_ * cos(d_theta) - cone.y_ * sin(d_theta);
            const auto drehung_y = cone.x_ * sin(d_theta) + cone.y_ * cos(d_theta);
            const auto x = n->pose_.pose_.x() + drehung_x;
            const auto y = n->pose_.pose_.y() + drehung_y;
            geometry_msgs::Point cone_point;
            cone_point.x = x;
            cone_point.y = y;
            marker.points.push_back(cone_point);

            marker.scale.x = 0.1;
            marker.scale.y = 0.1;
            marker.scale.z = 0.1;
            marker.color.a = 1.0; // Don't forget to set the alpha!
            marker.color.r = 0.0;
            marker.color.g = 1.0;

            pub_viz_.publish(marker);
        }
      }

    }
  }

} // namespace estimation