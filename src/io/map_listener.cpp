//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "map_listener.hpp"
namespace estimation{
MapListener::MapListener ( ESTIMATION::Map_Input_Params &params,std::function<void ( d4_comm_pkg::RawMap & ) > cb )
{
  cb_ = cb;
  params_ = params;
  switch ( params.plattform_ )
    {
    case ESTIMATION::PLATTFORM::D4:
    case ESTIMATION::PLATTFORM::MARINA:
      ROS_INFO_STREAM ( "MAP PLATTFORM D4/MARINA LISTENING TO " <<  params.topic_ );
      sub_ =  params.nh_->subscribe ( params.topic_,1,&MapListener::CBD4Map,this );
      break;
    case ESTIMATION::PLATTFORM::SIM:
      ROS_INFO_STREAM ( "MAP PLATTFORM SIM LISTENING TO " <<  params.topic_ );
      sub_ =  params.nh_->subscribe ( params.topic_,1,&MapListener::CBFSMap,this );
      break;
    }
}//endfunc
//****************************************************************

void MapListener::CBD4Map ( const d4_comm_pkg::RawMap_< std::allocator< void > >::ConstPtr& Track )
{
  map_ = Track.operator*();
  ROS_INFO_STREAM ( "MAP RECEIVED " );
  cb_ ( map_ );
}//endfunc
//****************************************************************

void MapListener::CBFSMap ( const fssim_common::Track_< std::allocator< void > >::ConstPtr& Track )
{
  ROS_INFO_STREAM ( "MAP RECEIVED " );
  AddToMapFromFSSIM(map_,Track.get()->cones_left);
  AddToMapFromFSSIM(map_,Track.get()->cones_right);
  AddToMapFromFSSIM(map_,Track.get()->cones_orange);
  AddToMapFromFSSIM(map_,Track.get()->cones_orange_big);
  ROS_INFO_STREAM ( "Size  " << map_.cones.size() );
  cb_ ( map_ );
}//endfunc
//****************************************************************

void MapListener:: AddToMapFromFSSIM(d4_comm_pkg::RawMap &map,const std::vector<geometry_msgs::Point> &source)
{
 for ( const auto &p : source)
    {
      d4_comm_pkg::Cone cony;
      cony.x = p.x;
      cony.y = p.y;
      cony.covariance.reserve(2);
      cony.covariance.push_back(params_.cov_y); 
      cony.covariance.push_back(params_.cov_x);
      map.cones.push_back ( cony );
    }
}//endfunc
//****************************************************************

}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 