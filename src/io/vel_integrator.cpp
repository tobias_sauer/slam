//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "vel_integrator.hpp"
namespace estimation{
  
void VelocityIntegrator::CallbackSpeedD4 ( const d4_comm_pkg::VehicleStateDT::ConstPtr& speed_in )
{
 
  AddOdometry ( speed_in->x,speed_in->y,speed_in->theta,speed_in->header.stamp );
}//endfunc
//****************************************************************

void VelocityIntegrator::CallbackSpeedSim ( const fssim_common::State_< std::allocator< void > >::ConstPtr& speed_in )
{
  AddOdometry ( speed_in->vx,speed_in->vy,speed_in->r,speed_in->header.stamp );
}//endfunc
//****************************************************************

void VelocityIntegrator::CallbackSpeedVehicleCom ( const vehicle_communication_pkg::VehicleState::ConstPtr& speed_in )
{
  AddOdometry ( speed_in->vel_x,speed_in->vel_y,speed_in->yaw_rate,speed_in->header.stamp );
}//endfunc
//****************************************************************

VelocityIntegrator::VelocityIntegrator ( ESTIMATION::Velocity_Params& params )
{
  params_ = params;
  ros::NodeHandle *nh = params.nh_;
  switch ( params.plattform_ )
    {
    case ESTIMATION::PLATTFORM::D4:
      sub_speed = nh->subscribe ( params.topic_,1,&VelocityIntegrator::CallbackSpeedD4,this );
      break;
    case ESTIMATION::PLATTFORM::MARINA:
      sub_speed = nh->subscribe ( params.topic_,1,&VelocityIntegrator::CallbackSpeedVehicleCom,this );
      break;
    case ESTIMATION::PLATTFORM::SIM:
      sub_speed = nh->subscribe ( params.topic_,1,&VelocityIntegrator::CallbackSpeedSim,this );
      break;
    }

}//endfunc
//****************************************************************



void VelocityIntegrator::AddOdometry ( double x, double y, double phi,ros::Time stamp )
{

  std::lock_guard<std::mutex> lk(mut);
  VelDataPoint data;
  data.t = stamp;
  data.vx =x;
  data.vy =y;
  data.vtheta = phi;
  if(data_points_.size())
  {
    if(data.t > data_points_.back().t)
    {
      data_points_.push_back(data);
      last_stamp = stamp;
    }
    auto now = ros::Time::now(); //todo make it once a sec or so
    while (data_points_.front().t < now - ros::Duration(5.0))
    {
      data_points_.pop_front();
    }
  }
  else
  {
    data_points_.push_back(data);
    last_stamp = stamp;
  }
}//endfunc
//****************************************************************

gtsam::Pose2 VelocityIntegrator::OdoFromTo ( ros::Time begin, ros::Time end )
{
  auto retval = gtsam::Pose2(0,0,0);

  if( begin < end)
  {
    retval = from_to ( begin,end );
  }
  return retval;
}//endfunc
//****************************************************************

gtsam::Pose2 VelocityIntegrator::OdoTillNow ( ros::Time begin )
{
  return from_to ( begin,last_stamp );
}//endfunc
//****************************************************************

gtsam::Pose2 VelocityIntegrator::from_to ( ros::Time begin, ros::Time end )
{
  std::lock_guard<std::mutex> lk(mut);
  double resolution = 0.00005; //in sec
  int milsec_factor = 1/(resolution*1000);
  if(data_points_.size() < 10) 
    return gtsam::Pose2(0,0,0);
  if(end < data_points_.front().t)
    return gtsam::Pose2(0, 0, 0);
  if (begin > data_points_.back().t)
    return gtsam::Pose2(0, 0, 0);
  if ((end-begin).toSec() < 0.001)
    return gtsam::Pose2(0, 0, 0);
  auto lower = GetLowerBound(data_points_,begin);
  auto upper = GetUpperBound(data_points_,end);

  if(upper == data_points_.end() ) upper--;

  std::vector<VelDataPoint> vec;
  size_t n = milsec_factor * GetMilsecFromDiff(upper->t - lower->t);
  if (upper == data_points_.end() - 1)
  {
    size_t n_f = milsec_factor * GetMilsecFromDiff(end - upper->t);
    vec.reserve(1.2*(n+n_f));
  }
  else
  {
    vec.reserve(1.2*n);
  }
  
  auto cur_it = std::begin(vec);

  for (auto it = lower; it != upper; ++it)
  {
    auto mill_between_two = milsec_factor * GetMilsecFromDiff((it + 1)->t - it->t);
    for (auto idx = 0; idx < mill_between_two; idx++)
    {
      cur_it++;

      auto obj = GetWeightedObject(*it, *(it + 1), ros::Duration((double)idx * resolution));
      vec.push_back(obj);
    }
  }
  if (upper->t < end)
  {
    size_t n_f = milsec_factor * GetMilsecFromDiff(end - upper->t);
    for (size_t idx = 0; idx < n_f; idx++)
    {
      VelDataPoint data;
      data.vtheta = data_points_.back().vtheta;
      data.vx = data_points_.back().vx;
      data.vy = data_points_.back().vy;
      data.t = data_points_.back().t + ros::Duration(idx* resolution);
      vec.push_back(data);
    }
  }
  auto vec_upper = GetUpperBound(vec, end);
  if (vec_upper == vec.end()) vec_upper--;
  auto vec_lower = GetLowerBound(vec, begin);
  if(vec_upper->t < vec_lower->t)
  {
    ROS_BREAK();
  }
  gtsam::Pose2 repos(0, 0, 0);
  auto t_integrated = 0.0;
  for(auto it = vec_lower; it != vec_upper ; it ++)
  {
    auto x = it->vx * resolution;
    auto y = it->vy * resolution;
    auto th = it->vtheta * resolution;
    t_integrated += resolution;
    repos = repos.compose(gtsam::Pose2(x,y,th));
  }
  return repos;
}//endfunc
//****************************************************************

std::deque<VelDataPoint>::const_iterator VelocityIntegrator::GetLowerBound(const std::deque<VelDataPoint> &c, const ros::Time &t) const
{
  if (t < c.front().t)
  {
    ROS_WARN_STREAM("Odo request requires older data than provided");
    return c.begin();
  }
  else if (t > c.back().t)
  {
    ROS_BREAK();
  }
  else
  {
    auto it = c.end();
    while (it != c.begin())
    {
      --it;
      if (t >= it.operator*().t)
      {
        return it;
      }
    }
  }
  ROS_BREAK();
}

std::deque<VelDataPoint>::const_iterator VelocityIntegrator::GetUpperBound(const std::deque<VelDataPoint> &c, const ros::Time &t) const
{
  if (t >= c.back().t)
  {
    ROS_WARN_STREAM("Odo request requires newer data than provided");
    return c.end();
  }
  else if (t < c.front().t)
  {
    ROS_ERROR_STREAM(" end time requestet older than oldest time point");
    ROS_ERROR_STREAM(" t upper bound requested " << t );
    ROS_ERROR_STREAM(" t oldest " << c.front().t);
    ROS_BREAK();
  }
  else
  {
    auto it = c.begin();
    while (it != c.end())
    {
      ++it;
      if (t < it.operator*().t)
      {
        return it;
      }
    }
  }
  ROS_BREAK();
}

std::vector<VelDataPoint>::const_iterator VelocityIntegrator::GetLowerBound(const std::vector<VelDataPoint> &c, const ros::Time &t) const
{
  if (t < c.front().t)
  {
    ROS_WARN_STREAM("Odo request requires older data than provided");
    return c.begin();
  }
  else if (t > c.back().t)
  {
    ROS_ERROR_STREAM(" end time requestet newer than newer time point");
    ROS_ERROR_STREAM(" t lower bound requested " << t);
    ROS_ERROR_STREAM(" t newest " << c.back().t);
    ROS_BREAK();
  }
  else
  {
    auto it = c.end();
    while (it != c.begin())
    {
      --it;
      if (t >= it.operator*().t)
      {
        return it;
      }
    }
  }
  ROS_BREAK();
}

std::vector<VelDataPoint>::const_iterator VelocityIntegrator::GetUpperBound(const std::vector<VelDataPoint> &c, const ros::Time &t) const
{
  if (t >= c.back().t)
  {
    ROS_WARN_STREAM("Odo request requires newer data than provided");
    return c.end();
  }
  else if (t < c.front().t)
  {
    ROS_BREAK();
  }
  else
  {
    auto it = c.begin();
    while (it != c.end())
    {
      ++it;
      if (t < it.operator*().t)
      {
        return it;
      }
    }
  }
  ROS_BREAK();
}

VelDataPoint VelocityIntegrator::GetWeightedObject(const VelDataPoint &a, const VelDataPoint &b, const ros::Duration &tstep) const 
{
    VelDataPoint retval;
    retval.t = a.t+tstep;
    retval.vx = WeightedValue(a.vx,b.vx,a.t,b.t,tstep);
    retval.vy = WeightedValue(a.vy, b.vy, a.t, b.t, tstep);
    retval.vtheta = WeightedValue(a.vtheta, b.vtheta, a.t, b.t, tstep);
    return retval;
}

size_t VelocityIntegrator::GetMilsecFromDiff(const ros::Duration &diff) const
{
  return std::floor((diff).toSec() * 1000);
}

float VelocityIntegrator::GetRatio(const ros::Time t1, const ros::Time t2, const ros::Duration &tstep) const
{
  auto t_diff = t2-t1;
  return (1.0- (t_diff-tstep).toSec()/t_diff.toSec());
} //endfunc
//****************************************************************

float VelocityIntegrator::WeightedValue(float val1,float val2, const ros::Time &t1, const ros::Time &t2, const ros::Duration &tstep) const
{
  auto ratio = GetRatio(t1, t2, tstep);
  auto first = val1 * (1.0 - ratio);
  auto second = val2 * ratio;
  return (first+second)/1.0;
} //endfunc
//****************************************************************

d4_comm_pkg::PoseVel VelocityIntegrator::GetSpeed()
{
  std::lock_guard<std::mutex> lk(mut);
  d4_comm_pkg::PoseVel retval;
  if( data_points_.size())
  {
    retval.header.stamp = data_points_.back().t;
    retval.State.x = data_points_.back().vx;
    retval.State.y = data_points_.back().vy;
    retval.State.theta = data_points_.back().vtheta;
  }
  return retval;
}//endfunc
//****************************************************************


}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
