//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "tf_pub.hpp"
namespace estimation{
TF_Pub::TF_Pub ( ESTIMATION::TF_Params& params )
{

  transform_map.setOrigin ( tf::Vector3 ( 0.0, 0.0, 0.0 ) );
  transform_map.setRotation ( tf::Quaternion ( 0, 0, 0, 1 ) );
  br_map.sendTransform ( tf::StampedTransform ( transform_map,ros::Time::now().now()
                         ,std::string ( "world" ),params.frame_map ) );

  transform_car.setOrigin ( tf::Vector3 ( 0.0, 0.0, 0.0 ) );
  transform_car.setRotation ( tf::Quaternion ( 0, 0, 0, 1 ) );
  br_map.sendTransform ( tf::StampedTransform ( transform_car,ros::Time::now().now()
                         ,params.frame_map,params.frame_car ) );


  params_ = params;
  std::thread t1 ( &TF_Pub::TF_Thread,this );
  t1.detach();
  ROS_INFO_STREAM("Started TF");
  ROS_INFO_STREAM("Started Base_link : " << params.frame_car );
  ROS_INFO_STREAM("Started world : " << params.frame_map );
}//endfunc
//****************************************************************


void TF_Pub::TF_Thread()
{
  for ( ;; )
    {
      std::this_thread::sleep_for ( std::chrono::milliseconds ( 20 ) );
      transform_map.setOrigin ( tf::Vector3 ( 0.0, 0.0, 0.0 ) );
      transform_map.setRotation ( tf::Quaternion ( 0, 0, 0, 1 ) );
      br_map.sendTransform ( tf::StampedTransform ( transform_map,ros::Time::now().now()
                             ,std::string ( "world" ),params_.frame_map ) );

      transform_car.setOrigin ( tf::Vector3 ( cur_pose_.x(), cur_pose_.y(), 0.0 ) );
      //transform_car.setOrigin ( tf::Vector3 ( 0, 0, 0.0 ) );
      tf::Quaternion q;
      q.setRPY ( 0,0,cur_pose_.theta() );
      //q.setEuler ( 0,0,0 );
      transform_car.setRotation ( q );
      br_map.sendTransform ( tf::StampedTransform ( transform_car,ros::Time::now().now()
                             ,params_.frame_map,params_.frame_car ) );

    }
}//endfunc
//****************************************************************

void TF_Pub::Update_TF ( gtsam::Pose2 cur_pose )
{
  cur_pose_ = cur_pose;
}//endfunc
//****************************************************************

}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
