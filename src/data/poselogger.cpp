//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "poselogger.hpp"

namespace estimation{
PoseLogger::PoseLogger ( ESTIMATION::Frontend_Params params )
{
  for(const auto &s : params.sensors)
  {
    if(s.logging == true)
    {
        logging_enabled_ = true;
        break;
    }
  }

  if(logging_enabled_ == true)
  {
    ROS_INFO_STREAM("Starting Poselogger");
    params_ = params;
    path_ = ros::package::getPath("estimation");
    path_ += "/log/";
    path_ += std::to_string(ros::WallTime::now().toSec());
    ROS_INFO_STREAM("Logging Path : ");
    ROS_INFO_STREAM(path_);
    std::ofstream file(path_); //open in constructor

    for(ESTIMATION::Sensor_Params &s : params_.sensors)
    {
      file << "Sensor " << s.id << " is " << s.name << '\n' ; 
    }
    file << "SensorID,Measurement x,Measurement y,Matched x,Matched y" << '\n' ; 
    std::thread t1 ( &PoseLogger::LoggingTread,this );
    t1.detach();
  }
}



void PoseLogger::Log ( Node& p, map< size_t, Measurement >& map )
{
  if(logging_enabled_ == true)
  {
    lock_.lock();
    map_ = &map;
    logging_queue_.push(p);
    lock_.unlock();
    signal_.Give();
  }
}

void PoseLogger::LoggingTread(){
  for(;;){
    signal_.Take();
    lock_.lock();
    while (logging_queue_.empty() == false){
      Node o = logging_queue_.front();
      logging_queue_.pop();
      LogSensor(o); 
    }
    lock_.unlock();
  }
}

void PoseLogger::LogSensor ( Node& p )
{
  std::ofstream file; //open in constructor
  file.open(path_,std::ios::app);
  /*for(Measurement &m:p.Meas_)
  {
    file << p.sensors_params.id << "," <<  m.x_ << "," << m.y_ << ',';
    if(m.key_meas_ != MAGIC::NUMBER)
    {
        Measurement lm = map_->at(m.key_meas_);
        file << lm.x_ << "," << lm.y_;
    }
    else
    {
      file << 0 << "," << 0;
    }
    file << '\n' ;
  }*/
}

}