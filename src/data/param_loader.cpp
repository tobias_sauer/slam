//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "param_loader.hpp"
namespace estimation{
void ParamLoader::LoadParams ( ESTIMATION::Params_S& params, ros::NodeHandle* nh )
{
  params.nh_ = nh;
  nh->getParam ( slam_namespace+"plattform",params.plattform );
  ROS_ASSERT_MSG((params.plattform <= 4), "ASSERT PARMAS NO PLATTFORM SELECETED");
  nh->getParam ( slam_namespace+"mode",params.mode );
  ROS_ASSERT_MSG((params.mode <= 3), "ASSERT PARMAS NO PLATTFORM SELECETED");

  nh->getParam ( slam_namespace+"graph_windowed_approach",params.window_mode );
  nh->getParam ( slam_namespace+"graph_window_size",params.window_size );
  params.pub_params.nh_ = nh;
  LoadPubParams ( params.pub_params,nh );


  params.frontend_params.nh_ = nh;
  params.frontend_params.plattform = params.plattform;

  nh->getParam ( slam_namespace+"frontend_max_key_frame_deviation",params.frontend_params.max_key_frame_deviation );
  nh->getParam ( slam_namespace+"mode",params.frontend_params.mode );
  LoadFrotendParams ( params.frontend_params,nh );

  if ( params.plattform != ESTIMATION::PLATTFORM::SIM )
    {
      LoadTFParams ( params.tf_params,nh );
    }
  LoadGraphParams ( params.graph_params,nh );

  nh->getParam ( slam_namespace+"da_csquare",params.frontend_params.da_params.chi_sq );
  nh->getParam(slam_namespace + "da_map_count", params.frontend_params.da_params.count);
  nh->getParam(slam_namespace + "da_map_ttl", params.frontend_params.da_params.ttl);
  LoadGPSparams(params,nh);
}

void ParamLoader::LoadDebugFlags ( ros::NodeHandle *nh )
{
    nh->getParam(slam_namespace+"DEBUG_LOCK", ESTIMATION::DEBUG_LOCK);
    nh->getParam(slam_namespace+"DEBUG_NNDA", ESTIMATION::DEBUG_NNDA);
    nh->getParam(slam_namespace+"DEBUG_NNDA_COV_TABLE", ESTIMATION::DEBUG_NNDA_COV_TABLE);
    nh->getParam(slam_namespace+"DEBUG_MATCHES_LIGHT", ESTIMATION::DEBUG_MATCHES_LIGHT);
    nh->getParam(slam_namespace+"DEBUG_MATCHES_FULL", ESTIMATION::DEBUG_MATCHES_FULL);
    nh->getParam(slam_namespace+"DEBUG_NUMPOSES", ESTIMATION::DEBUG_NUMPOSES);
    nh->getParam(slam_namespace+"DEBUG_LOCK", ESTIMATION::DEBUG_LOCK);
}

void ParamLoader::LoadGPSparams(ESTIMATION::Params_S &params, ros::NodeHandle *nh)
{
  params.frontend_params.gpsparams_.plattform = params.plattform;
  params.frontend_params.gpsparams_.nh_ = nh;
  nh->getParam(slam_namespace + "gps_on", params.frontend_params.gps_activated);
  nh->getParam(slam_namespace + "gps_topic", params.frontend_params.gpsparams_.topic);
  nh->getParam(slam_namespace + "gps_frequency", params.frontend_params.gpsparams_.frequency);
  nh->getParam(slam_namespace + "gps_cov_x",params.frontend_params.gpsparams_.cov_x );
  nh->getParam(slam_namespace + "gps_cov_y", params.frontend_params.gpsparams_.cov_y);
  nh->getParam(slam_namespace + "gps_cov_theta", params.frontend_params.gpsparams_.cov_theta);
}

void ParamLoader::LoadPubParams(ESTIMATION::Pub_Params &params, ros::NodeHandle *nh)
{
  nh->getParam ( slam_namespace+"viz_map_topic",params.topic_viz_finalised_map );
  nh->getParam ( slam_namespace+"viz_display_temp_map",params.topic_viz_temporary_map );
  nh->getParam ( slam_namespace+"viz_pos_topic",params.topic_viz_pos_map );


  nh->getParam ( slam_namespace+"pub_pos_topic",params.topic_position );


  nh->getParam ( slam_namespace+"map_link",params.frame_map );
  nh->getParam ( slam_namespace+"base_link",params.frame_car );

  nh->getParam ( slam_namespace+"viz_display_pose",params.viz_pos );
  nh->getParam ( slam_namespace+"viz_display_map",params.viz_finalised_map );
  nh->getParam ( slam_namespace+"viz_display_temp_map",params.viz_temporary_map );
}

void ParamLoader::LoadFrotendParams ( ESTIMATION::Frontend_Params& params, ros::NodeHandle* nh )
{
  nh->getParam ( slam_namespace+"sensor_count",params.number_sensors );
  ROS_ASSERT_MSG(params.number_sensors >= 1 , "ASSERT NO SENSOR INPUTS IN PARAMS");
  nh->getParam ( slam_namespace+"frontend_ttl",params.TTL );
  std::string base_link;
  nh->getParam ( slam_namespace+"base_link",base_link );
  for(size_t i=1; i < params.number_sensors +1; i++)
  {
    ESTIMATION::Sensor_Params sensor;
    sensor.base_link = base_link;
    nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) + "_id"),sensor.id );
    nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_type"),sensor.type );
    nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_topic"),sensor.topic );
    nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_name"),sensor.name );
    nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_unary_feature_matching"),sensor.unary_feature_matching ); 
    nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_external_covar_given"),sensor.covar_given );
    if ( sensor.covar_given == false )
    {
      nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_cov_r"),sensor.covar_b );
      nh->getParam ( std::string(slam_namespace+"s" + std::to_string(i) +"_cov_b"),sensor.covar_r );
    }
    sensor.nh_ = nh;
    sensor.plattform = params.plattform;
    params.sensors.push_back ( sensor );
    ROS_INFO_STREAM("Params Loaded for Sensor " << sensor.name << " with id" << sensor.id );
  }

  params.velocity_params.nh_ = nh;
  params.velocity_params.plattform_ = params.plattform;
  nh->getParam ( slam_namespace+"vel_topic",params.velocity_params.topic_ );
  nh->getParam(slam_namespace + "vel_cov_x", params.velocity_params.cov_x);
  nh->getParam(slam_namespace + "vel_cov_y", params.velocity_params.cov_y);
  nh->getParam(slam_namespace + "vel_cov_theta", params.velocity_params.cov_thetha);

  params.map_input_params_.nh_ = nh;
  params.map_input_params_.plattform_ = params.plattform;
  nh->getParam ( slam_namespace+"map_topic",params.map_input_params_.topic_ );
  nh->getParam( slam_namespace+"map_external_covar_given", params.map_input_params_.external_covar_given);
  if(params.map_input_params_.external_covar_given == false)
  {
     nh->getParam( slam_namespace+"map_cov_x", params.map_input_params_.cov_x);
     nh->getParam( slam_namespace+"map_cov_y", params.map_input_params_.cov_y);
  }

  nh->getParam ( slam_namespace+"start_x",params.start_x );
  nh->getParam ( slam_namespace+"start_y",params.start_y );
  nh->getParam ( slam_namespace+"start_theta",params.start_theta );
  nh->getParam ( slam_namespace+"wait_for_res",params.wait_for_res );
  
}

void ParamLoader::LoadGraphParams ( ESTIMATION::Graph_Params& params, ros::NodeHandle* nh )
{
  params.nh_ = nh;
  nh->getParam ( slam_namespace+"start_x",params.start_x );
  ROS_INFO_STREAM(" Slam start x " << params.start_x);
  ROS_INFO_STREAM(" Slam start y " << params.start_y);
  nh->getParam ( slam_namespace+"start_y",params.start_y );
  nh->getParam ( slam_namespace+"start_theta",params.start_theta );

  nh->getParam ( slam_namespace+"graph_l_kf",params.localization_keyframes );
  nh->getParam ( slam_namespace+"graph_l_ng",params.localization_new_graph_after );

  nh->getParam ( slam_namespace+"graph_m_kf",params.mapping_keyframes );
  nh->getParam ( slam_namespace+"graph_m_ng",params.mapping_new_graph_after );

  nh->getParam ( slam_namespace+"localization_mode",params.graph_mode_localization );
  ROS_INFO_STREAM ( "local mode " << params.graph_mode_localization );
  nh->getParam ( slam_namespace+"graph_con_lifetime",params.graph_con_lifetime );
}



void ParamLoader::LoadTFParams ( ESTIMATION::TF_Params& params, ros::NodeHandle* nh )
{
  nh->getParam ( slam_namespace+"base_link",params.frame_car );
  nh->getParam ( slam_namespace+"map_link",params.frame_map );
  params.nh_ = nh;

  nh->getParam ( slam_namespace+"start_x",params.start_x );
  nh->getParam ( slam_namespace+"start_y",params.start_y );
  nh->getParam ( slam_namespace+"start_theta",params.start_theta );
}
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
