//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "pose_object.hpp"

namespace estimation{



const std::vector<Measurement> Node::GetMeasuremnts()  const
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  return Meas_;
} //endfunc
//****************************************************************

void Node::UpdateMeasurementKeys(const std::vector<Measurement> &meas)  
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  auto keys_added = 0;
  for(const auto &source: meas)
  {
    for(auto &target : Meas_)
    {
      if(source.x_ == target.x_ && source.y_ == target.y_)
      {
        target.key_meas_ = source.key_meas_;
        keys_added ++;
      }
    }
  }
  if(keys_added != meas.size())
  {
    ROS_ERROR_STREAM("Could find All keys while updating Measurement!");
  }
  measurments_accesed_by_graph_id.clear();
  data_associated = true;
} //endfunc
//****************************************************************

bool Node::IsDataAsociated() const
{
  return data_associated;
} //endfunc
//****************************************************************

size_t Node::GetNumberOfMeasurements() const
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  return Meas_.size();
} //endfunc
//****************************************************************

void Node::AddMeasurements(const std::vector<Measurement> &meas)  
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  for(const auto &source : meas) Meas_.push_back(source);
  //invalidate all
  for( auto &meas : Meas_)
  {
    meas.key_meas_ = MAGIC::NUMBER;
  }
  data_associated = false;
} //endfunc
//****************************************************************

bool Node::IsGps()const
{
  return is_gps_pose_;
} //endfunc
//****************************************************************

Eigen::Matrix3f Node::GetGPSCovar() const
{
  return gps_cov_;
} //endfunc
//****************************************************************

void Node::SetGpsPose(gtsam::Pose2 gps_pose, Eigen::Matrix3f cov)
{
  gps_cov_ = cov;
  is_gps_pose_ = true;
  pose_.pose_ = gps_pose;
} //endfunc
//****************************************************************

Measurement::Measurement(const d4_comm_pkg::Cone &c)
: x_(c.x), y_(c.y)
{
  key_meas_ = MAGIC::NUMBER;
  meas_covar_ ( 0,0 ) = c.covariance.at ( 0 );
  if(c.covariance.size() == 2)
  {
    meas_covar_ ( 1,1 ) = c.covariance.at ( 1 );
  }
  else
  {
    meas_covar_ ( 1,1 ) = c.covariance.at ( 3 );
  }
} //endfunc
//****************************************************************

bool Node::IsMeasUsed ( std::size_t id )
{
  if ( std::find ( measurments_accesed_by_graph_id.begin(),measurments_accesed_by_graph_id.end(),id ) ==measurments_accesed_by_graph_id.end() )
    {
      return false;
    }
  else
    {
      return true;
    }
  measurments_accesed_by_graph_id.push_back ( id );
} //endfunc
//****************************************************************

void Node::SetMeasUsed ( std::size_t id )
{
  if (std::find(std::begin(measurments_accesed_by_graph_id), std::end(measurments_accesed_by_graph_id), id) == std::end(measurments_accesed_by_graph_id))
  {
    measurments_accesed_by_graph_id.push_back(id);
  }
} //endfunc
//****************************************************************

bool Node::IsPoseUsed ( std::size_t id )
{
  if ( std::find ( poses_accesed_by_graph_id.begin(),poses_accesed_by_graph_id.end(),id ) ==poses_accesed_by_graph_id.end() )
    {
      return false;
    }
  else
    {
      return true;
    }
} //endfunc
//****************************************************************

void Node::SetPosUsed ( std::size_t id )
{
  if (std::find(std::begin(poses_accesed_by_graph_id),std::end(poses_accesed_by_graph_id),id) == std::end(poses_accesed_by_graph_id))
  {
    poses_accesed_by_graph_id.push_back ( id );
  }
} //endfunc
//****************************************************************

void Node::DEBUG_PRINT_ALL_LANDMARKS() const
{
  for ( size_t i=0 ; i< Meas_.size() ; i++ )
    {
      ROS_INFO_STREAM ( "MEAS X : "<<  Meas_[i].x_  << " MEAS Y : " << Meas_[i].y_ );
    }
} //endfunc
//****************************************************************

void Node:: DEBUG_PRINT_MATCHED_LANDMARKS() const
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  for ( size_t i=0 ; i< Meas_.size() ; i++ )
    {
      if ( Meas_[i].key_meas_ != MAGIC::NUMBER )
        {
          ROS_INFO_STREAM ( "MEAS X : "<<  Meas_[i].x_  << " MEAS Y : " << Meas_[i].y_ );
        }
    }
    std::vector<int> a;
} //endfunc
//****************************************************************

void Node::DEBUG_PRINT_POS() const
{
  ROS_INFO_STREAM ( "POS X : "<<  pose_.pose_.x()  << " POS Y : " << pose_.pose_.y() << " POS Theta : " << pose_.pose_.theta() );
} //endfunc
//****************************************************************

void Node::DEUBG_PRINT_MEAS_AND_CONTAINING_LM (  std::map< size_t, Measurement >  & local_map_ ) 
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  const auto d_theta = pose_.pose_.theta();
  for ( const auto &m  :  Meas_ )
    {
      if ( m.key_meas_ != MAGIC::NUMBER )
        {
          // first transform
          const auto drehung_x = m.x_ *cos ( d_theta ) - m.y_*sin ( d_theta );
          const auto drehung_y  = m.x_ *sin ( d_theta ) + m.y_* cos ( d_theta );
          const auto x = pose_.pose_.x()  + drehung_x;
          const auto y = pose_.pose_.y()  + drehung_y;
          ROS_INFO_STREAM ( "  " );
          DEBUG_PRINT_POS();
          ROS_INFO_STREAM ( "MEAS X : "<<  m.x_ << " MEAS Y : " << m.y_ );
          ROS_INFO_STREAM ( "MEAS X TRANS : "<<  x  << " MEAS Y TRANS: " << y );
          ROS_INFO_STREAM("LM X : " << local_map_.at(m.key_meas_).x_ << " LM Y : " << local_map_.at(m.key_meas_).y_);
          ROS_INFO_STREAM ( "  " );
        }
    }
} //endfunc
//****************************************************************

void Node::DEBUG_PRINT_ODO() const
{
  //ROS_INFO_STREAM ( "odo X : "<<  odo_.x()  << " odo Y : " << odo_.y() << " odo Theta : " << odo_.theta() );
} //endfunc
//****************************************************************

void Node::DEBUG_PRINT_COV_POS() const
{
  std::lock_guard<std::mutex> lk(*meas_mut_);
  ROS_INFO_STREAM ( "cov X : "<<  pose_.pos_covar_ ( 0,0 )  << " odo Y : " << pose_.pos_covar_ ( 1,1 ) << " odo Theta : " << pose_.pos_covar_ ( 2,2 ) );
} //endfunc
//****************************************************************
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
