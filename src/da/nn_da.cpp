//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#include "nn_da.hpp"
namespace estimation
{
  using namespace Eigen;
  using namespace boost::math;

  NN_DA::NN_DA(ESTIMATION::DA_Params &params)
  {
    params_ = params;
  }//end func
  //*****************************************************************

  std::vector<Measurement> NN_DA::NNDataAssociation(const Node &obj) const
  {
    auto t_now = ros::WallTime::now().toSec();
    const auto retval = Normal_Da(obj,local_map_);
    t_now = ros::WallTime::now().toSec() - t_now;
    ROS_INFO_STREAM(" Calc Time NN DA" << t_now);
    return retval;
  } //end func
  //*****************************************************************

  std::vector<Measurement> NN_DA::MappingDataAssociation(const Node &obj)
  {
    std::map<uint64_t, estimation::Measurement> local_map;
    {
      std::lock_guard<std::mutex> lock(map_mutex_);
      local_map = local_map_; //create working copy for concourrency
      AddCandidatesToMap(local_map);
      ROS_ERROR_STREAM(" map size = " << local_map_.size());
      ROS_INFO_STREAM(" before g8 " << local_map.size());
      local_map = GateMap(obj, local_map, 25.0f);
    
    ROS_INFO_STREAM("lm siz " << local_map.size());
    const auto results = Normal_Da(obj,local_map); //this takes the most time and doesnt need a lock

      const auto candidates = ExtractCandidatesFromResults(results);
      ProcessCandidates(obj,candidates);
      auto not_matched = GetNotMachtedResults(obj,results);
      ROS_INFO_STREAM("size not machte " << not_matched.size());
      AddNotMatchedResultsToCandidates(not_matched,obj);
      return GetResultsWithoutCanidates(results);
    }
  } //end func
  //*****************************************************************

  void NN_DA::AddNotMatchedResultsToCandidates( std::vector<Measurement> &results, const Node &n)
  {
    for(auto &r : results)
    {
      auto key = gtsam::Symbol('c', canidate_keys++);
      r.key_meas_ = key;
      const auto transformed = TransformMeasurementToWorldCoordinates(n.pose_.pose_,r);
      r.x_ = transformed[0];
      r.y_ = transformed[1];
      Candidate c(r);
      candidates_.push_back(c);
    }
    ROS_WARN_STREAM(" candidates size " << candidates_.size());
  } //end func
  //*****************************************************************

  void NN_DA::AddCandidatesToMap(map<uint64_t, Measurement> &map)
  {
    for(const auto &c : candidates_)
    {
      map.insert({c.meas.key_meas_,c.meas});
    }
    ROS_WARN_STREAM("added c : m size " << map.size());
  } //end func
  //*****************************************************************

  std::vector<Measurement> NN_DA::GetNotMachtedResults(const Node &obj, const std::vector<Measurement> &results) const
  {
    std::vector<Measurement> retval;
    for (const auto &r : results)
    {
      if(r.key_meas_==MAGIC::NUMBER)
      {
        retval.push_back(r);
      }
    }
    return retval;
  } //end func
  //*****************************************************************

  std::vector<Measurement> NN_DA::ExtractCandidatesFromResults(const std::vector<Measurement> &results) const
  {
    auto min = gtsam::Symbol('c',0);
    auto max = gtsam::Symbol('c', 10000);
    std::vector<Measurement> retval;
    for(const auto &r : results)
    {
      if(r.key_meas_ < max && r.key_meas_>= min)
      {
        retval.push_back(r);
      }
    }
    return retval;
  } //end func
  //*****************************************************************

  std::vector<Measurement> NN_DA::GetResultsWithoutCanidates(const std::vector<Measurement> &results) const
  {
    auto min = gtsam::Symbol('l', 0);
    auto max = gtsam::Symbol('l', 10000);
    std::vector<Measurement> retval;
    for (const auto &r : results)
    {
      if (r.key_meas_ < max && r.key_meas_ >= min)
      {
        retval.push_back(r);
      }
    }
    return retval;
  } //end func
  //*****************************************************************

  void NN_DA::AddCandidateToMap(const Node &obj, Candidate &c, const Measurement &m)
  {
    const auto key = gtsam::Symbol('l', next_map_key++);
    const auto candidate_cov = c.meas.meas_covar_.sum();
    const auto meas_cov = c.meas.meas_covar_.sum();
    for (const auto &mapelem : local_map_)
    {
      const auto &meas = mapelem.second;
      if(std::hypot(meas.x_-c.meas.x_,meas.y_-c.meas.y_) < 1.5)
      {
        return;
      }
    }
    if (candidate_cov < meas_cov)
    {
      auto to_map = c.meas;
      to_map.key_meas_ = key;
      local_map_.insert({key,to_map});
    }
    else
    {
      auto to_map = m;
      auto transformed = TransformMeasurementToWorldCoordinates(obj.pose_.pose_, m);
      to_map.x_ = (c.meas.x_ + transformed[0]) / 2;
      to_map.y_ = (c.meas.y_ + transformed[1]) / 2;
      to_map.key_meas_ = key;
      local_map_.insert({key, to_map});
      ret_candidates_.insert({key, to_map});
    }
  } //end func
  //*****************************************************************

  void NN_DA::ProcessCandidates(const Node &obj, const std::vector<Measurement> &results)
  {
    std::vector<Candidate> new_candidates;
    for(auto &c : candidates_)
    {
      c.TTL--;
      const auto target_it = std::find_if(results.begin(), results.end(), [&](const Measurement &a) { return a.key_meas_ == c.meas.key_meas_; });
      if (target_it != std::end(results))
      {
        c.times_found++;
        c.found_by_.push_back(obj.sensors_params.id);
        if (std::set<int>(c.found_by_.begin(), c.found_by_.end()).size() >= 2 && c.times_found > 5)
        {
          AddCandidateToMap(obj,c,*target_it);
        }
        else if (c.TTL > 0)
        {
          auto transformed = TransformMeasurementToWorldCoordinates(obj.pose_.pose_, *target_it);
          c.meas.x_ = (c.meas.x_ + transformed[0] )/2;
          c.meas.y_ = (c.meas.y_ + transformed[1]) / 2;
          new_candidates.push_back(c);
        }
      }
      else
      {
        if(c.TTL > 0)
        {
          new_candidates.push_back(c);
        }
      }  
    }
    candidates_ = new_candidates;
  } //end func
  //*****************************************************************

  void NN_DA::Update_Map(const map<uint64_t, Measurement> &map_in)
  {
    std::lock_guard<std::mutex> lock(map_mutex_);
    for (const auto &elem : map_in)
    {
      local_map_.insert(elem);
    }
  } //end func
  //*****************************************************************

  std::vector<Measurement> NN_DA::Normal_Da(const Node &obj, const map<uint64_t, Measurement> &map)const
  {
    const auto gated_map = GateMap(obj, map, 25.0f); //reduce map range to 25m around cur pos
    auto meas = obj.GetMeasuremnts();
    vector<vector<float>> d2_matrix(meas.size());
    for (auto &v : d2_matrix)
    {
      v.reserve(gated_map.size());
    }
    MahalabonisDistance(d2_matrix, obj, gated_map, meas);
    std::vector<int> hungarian_assignment_result;
    HungarianAlgorithm Hungarian;
    Hungarian.Solve(d2_matrix, hungarian_assignment_result);
    size_t matches = 0;
    double cs = boost::math::quantile(boost::math::chi_squared(2), params_.chi_sq);
    for (size_t j = 0; j < hungarian_assignment_result.size(); j++)
    {
      auto k = hungarian_assignment_result[j];
      if(k!= -1)
      {
        if (std::abs(d2_matrix[j][k]) < cs)
        {
          matches++;
          auto it_map = gated_map.begin();
          std::advance(it_map, k);
          meas[j].key_meas_ = it_map->first;
        }
      }
    }
    ROS_INFO_STREAM_COND(ESTIMATION::DEBUG_MATCHES_LIGHT, "Matches " << matches << " of " << hungarian_assignment_result.size() << " measurements");
    return meas;
  } //end func
  //*****************************************************************

  void NN_DA::SetMap(const map<uint64_t, Measurement> &map_in)
  {
    std::lock_guard<std::mutex> lock(map_mutex_);
    local_map_ = map_in;
    ROS_INFO_STREAM("DA Map size: " << local_map_.size());
  } //end func
  //*****************************************************************

  std::map<uint64_t, Measurement> NN_DA::GateMap(const Node &obj, const std::map<uint64_t, Measurement> &map, float radius) const
  {
    std::map<uint64_t, Measurement> retval;
    for (const auto &p : map)
    {
      const auto distance = std::hypot(std::abs(p.second.x_) - std::abs(obj.pose_.pose_.x()), std::abs(p.second.y_) - std::abs(obj.pose_.pose_.y()));
      if (distance < radius)
      {
        //ROS_INFO_STREAM("GATE x " << p.second.x_  << "GATE y " << p.second.y_ );
        retval.insert(p);
      }
    }
    return retval;
  } //end func
  //*****************************************************************

  void NN_DA::MahalabonisDistance(vector<vector<float>> &d2_matrix, const Node &obj, const std::map<uint64_t, Measurement> &map, const std::vector<Measurement> &measurements) const
  {
    Eigen::Matrix2f pos_covar;
    pos_covar(0, 0) = obj.pose_.pos_covar_(0, 0);
    pos_covar(1, 1) = obj.pose_.pos_covar_(1, 1);
    pos_covar(1, 0) = 0;
    pos_covar(0, 1) = 0;
    ROS_ERROR_STREAM(" pos covar matching " << pos_covar);
    //assign the D²_ij Mattrix
    size_t i = 0;
    for (const auto &m : measurements)
    {
      auto vec_meas = TransformMeasurementToWorldCoordinates(obj.pose_.pose_.x(), obj.pose_.pose_.y(), obj.pose_.pose_.theta(),m.x_,m.y_);
      double range = std::hypot(m.x_, m.y_);
      double bearing = atan2(m.y_, m.x_);
      auto meas_cov = TransformEulertoEuclidianCov(m.meas_covar_,range,bearing);
      for (const auto &l : map)
      {
        //build difference vector + add up covar
        const auto &landmark = l.second;
        Vector2f vec_lm;
        vec_lm << landmark.x_, landmark.y_;
        Matrix2f overall_cov;
        overall_cov = meas_cov + pos_covar;
        /*
        if (landmark.meas_covar_(0, 0) <= 0 || landmark.meas_covar_(1, 1) <= 0)
        {
          overall_cov = meas_cov + pos_covar;
        }
        else
        {
          overall_cov = meas_cov * landmark.meas_covar_+ pos_covar;
        }
        */
        Vector2f vec_dif = vec_lm- vec_meas;
        vec_dif = vec_dif.cwiseAbs();

        try{
          auto d_2 = CalcluateMD(vec_dif,overall_cov);
          //d_2 = UnaryFeatureMatching(obj.sensors_params.unary_feature_matching,d_2,m.color,l.second.color);
          d2_matrix[i].push_back(d_2);
        }
        catch (...)
        {
          ROS_ERROR_STREAM(" landmark "<< l.second.x_ << " " << l.second.y_);
          ROS_ERROR_STREAM(" lm cov "  << landmark.meas_covar_);
          ROS_ERROR_STREAM(" pos   covar " << pos_covar);
          obj.pose_.pose_.print();
          ROS_ERROR_STREAM(" vec pos " << obj.pose_.pose_.x() << obj.pose_.pose_.y());
          ROS_BREAK();
        }
      }
      i++;
    }
  } //end func
  //*****************************************************************

  map<uint64_t, Measurement> NN_DA::GetNewLandmarks()
  {
    std::lock_guard<std::mutex> lock(map_mutex_);
    map<uint64_t, Measurement> retval = ret_candidates_;
    ret_candidates_.clear();
    return retval;
  }

  Matrix2f NN_DA::TransformEulertoEuclidianCov(const Matrix2f &eul_cov, float range, float bearing) const
  {
    Matrix2f jacobian;
    jacobian(0, 0) = cos(bearing);
    jacobian(1, 0) = sin(bearing);
    jacobian(0, 1) = -range * sin(bearing);
    jacobian(1, 1) = range * cos(bearing);
    //https://stackoverflow.com/questions/16585980/variance-matrix-from-polar-to-cartesian-coordinates
    Matrix2f retval = jacobian * eul_cov * jacobian.transpose();

    if (retval(0, 0) < 0 || retval(1, 1) < 0)
    {
      ROS_ERROR_STREAM("kartesian cov(calculated) : " << retval);
      ROS_ERROR_STREAM("speric cov : " << eul_cov);
      ROS_ERROR_STREAM("jacobian  : " << jacobian);
      std::__throw_invalid_argument("measurement covariance smaller than 0");
    }

    return retval;
  } //end func
  //*****************************************************************

  Vector2f NN_DA::TransformMeasurementToWorldCoordinates(const gtsam::Pose2 &pose, const Measurement &m) const 
  {
      return TransformMeasurementToWorldCoordinates(pose.x(),pose.y(),pose.theta(),m.x_,m.y_);
  } //end func

  Vector2f NN_DA::TransformMeasurementToWorldCoordinates(float p_x, float p_y, float p_theta, float t_x, float t_y) const
  {
    Vector2f retval;
    const auto d_theta = p_theta;
    const auto drehung_x = t_x * cos(d_theta) - t_y * sin(d_theta);
    const auto drehung_y = t_x * sin(d_theta) + t_y * cos(d_theta);
    const auto x = p_x + drehung_x;
    const auto y = p_y + drehung_y;
    retval << x, y;
    return retval;
  } //end func
  //*****************************************************************

  float NN_DA::CalcluateMD(const Vector2f &diff_vec, const Matrix2f covar) const
  {
    if (covar.determinant() == 0)
    {
      ROS_ERROR_STREAM(" overall singular covariance : " << covar);
      std::__throw_runtime_error("Singular covariance Mat");
    }
    
    float d_2 = diff_vec.transpose() * covar.inverse() * diff_vec;
    if (isnan(d_2))
    {
      std::__throw_runtime_error("nan value in mahalanobis distance");
    }
    if (d_2 < 0 || std::hypot(diff_vec[0], diff_vec[1]) > 1.5)
    {
      d_2 = 1000;
    }
    if( d_2 < 0)
    {
      ROS_ERROR_STREAM(" d2 " << d_2);
      ROS_ERROR_STREAM(" neg d2 cov : " << covar);
      ROS_ERROR_STREAM(" neg d2 cov : " << covar.inverse());
      ROS_ERROR_STREAM(" neg d2 vec : " << diff_vec);
      std::__throw_runtime_error("negative d2");
    }
      return d_2;
  } //end func
  //*****************************************************************

  float NN_DA::UnaryFeatureMatching(bool enabled, float d_2, int source_color, int target_color) const
  {
    float retval=d_2;
    if (enabled)
    {
      if (source_color != 8) //TODO name it
      {
        if (source_color != target_color)
        {
          retval = d_2 * 100;
        }
      }
      return retval;
    }
    else
    {
      return retval;
    }
  } //end func
  //*****************************************************************

} // namespace estimation

// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
