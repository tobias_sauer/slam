#include <logical_graph.hpp>

namespace estimation{

  LogicalGraph::LogicalGraph(std::shared_ptr<VelocityIntegrator> vel_integrator, ESTIMATION::Frontend_Params &params)
  {
    if (vel_integrator)
    {
      vel_integrator_ = vel_integrator;
    }
    else
    {
      ROS_FATAL("no velocity integrator provided");
      ROS_BREAK();
    }
    params_  = params;
    this->pose_keys_ = gtsam::symbol('x', 0);
    covar_odo_(0, 0) = params.velocity_params.cov_x;
    covar_odo_(1, 1) = params.velocity_params.cov_y;
    covar_odo_(2, 2) = params.velocity_params.cov_thetha;
}

std::shared_ptr<Node> LogicalGraph::AddMeasurementToGraph(const ros::Time &stamp){
  std::lock_guard<std::mutex> lk(graph_lock_);
  return FindCreateMatchingPose(stamp);
}

std::shared_ptr<Node> LogicalGraph::CreateReturnInbetweenNode(const ros::Time &stamp)
{
  std::shared_ptr<Node> predecessor{nullptr};
  std::shared_ptr<Node> sucessor{nullptr};
  EvaluateNeighbours(stamp, predecessor, sucessor);
  std::shared_ptr<Node> retval{nullptr};
  if (predecessor != nullptr && sucessor != nullptr)
  {
    //ROS_INFO_STREAM("Creating Inbetween ");
    retval = CreateInbetweenNode(stamp, predecessor, sucessor);
    //ROS_INFO_STREAM("INBETWEEN created");
  }
  else if (sucessor != nullptr)
  {
    retval = CreateNewFirstNodeWithSucessor(stamp, sucessor);
    ROS_WARN_STREAM("First Node With Sucessor");
  }
  else if (predecessor != nullptr)
  {
    ROS_ERROR_STREAM("Something strange happened precessor != nullptr, sucessor == nullptr");
  }
  else
  {
    ROS_ERROR_STREAM("Error,could not find predecessor");
  }
  return retval;
} //end func
//****************************************************************

std::shared_ptr<Node> LogicalGraph::CreateInbetweenNode(const ros::Time &stamp, const std::shared_ptr<const Node> &predecessor, const std::shared_ptr<const Node> &sucessor)
{
  const auto next_key = GetNextKey();
  // Create Pose
  const auto odo_pred = vel_integrator_->OdoFromTo(predecessor->pose_.time_, stamp);
  const auto integrated_pose = predecessor->pose_.pose_.compose(odo_pred); //initial estimate by Integrating Pose
  Pose p_target(integrated_pose, stamp, next_key);
  // Create Predecessor Edge
  Edge pred(predecessor->pose_.key_pose_, next_key, predecessor->pose_.time_, odo_pred,covar_odo_);
  // Create Sucsessor Edge
  const auto odo_suc = vel_integrator_->OdoFromTo(stamp, sucessor->pose_.time_);
  Edge suc(next_key, sucessor->pose_.key_pose_, stamp, odo_suc, covar_odo_);
  std::vector<Edge> edges{pred, suc};
  Node newnode(edges, p_target);
  newnode.pose_.pos_covar_ = predecessor->pose_.pos_covar_;
  auto ptrnode = std::make_shared<Node>(newnode);
  pose_object_container_.push_back(ptrnode);
  pose_object_map.emplace(ptrnode->pose_.key_pose_, ptrnode);
  return ptrnode;

} //end func
//****************************************************************

std::shared_ptr<Node> LogicalGraph::CreateNewFirstNodeWithSucessor(const ros::Time &stamp, const std::shared_ptr<const Node> &sucessor)
{
  const auto next_key = GetNextKey();
  // Create Pose
  const auto integrated_pose = sucessor->pose_.pose_;
  Pose p_target(integrated_pose, stamp, next_key);
  // Create Predecessor Edge
  const auto odo_suc = vel_integrator_->OdoFromTo(stamp, sucessor->pose_.time_);
  Edge suc(next_key, sucessor->pose_.key_pose_, stamp, odo_suc, covar_odo_);
  std::vector<Edge> edges{suc};
  Node newnode(edges, p_target);
  newnode.pose_.pos_covar_.setZero();
  auto ptrnode = std::make_shared<Node>(newnode);
  pose_object_container_.push_back(ptrnode);
  pose_object_map.emplace(ptrnode->pose_.key_pose_, ptrnode);
  return ptrnode;

} //end func
//****************************************************************

gtsam::Key LogicalGraph::GetNextKey()
{
  pose_keys_++;
  return pose_keys_;
} //end func
//****************************************************************

bool LogicalGraph::WithinLimits(const ros::Time &t1, const ros::Time &t2, const ros::Duration &deviation) const
{
  if (t1 < t2 + deviation && t1 > t2 - deviation)
    return true;
  else
    return false;
} //end func
  //****************************************************************

std::shared_ptr<Node> LogicalGraph::FindKeyPose(const ros::Time &stamp)
{
  std::shared_ptr<Node> retval{nullptr};
  ros::Duration d(params_.max_key_frame_deviation);
  for (auto &o : pose_object_container_)
  {
    if (WithinLimits(o->pose_.time_, stamp, d) == true)
    {
      retval = o;
      break;
    }
  }
  return retval;
} //end func
//****************************************************************

void LogicalGraph::EvaluateNeighbours(const ros::Time &stamp, std::shared_ptr<Node> &predecessor, std::shared_ptr<Node> &sucessor)
{
  //ROS_WARN_STREAM("my time " << stamp);
  for (auto &o : pose_object_container_)
  {
    if (o->pose_.time_ < stamp)
    {
      if (predecessor != nullptr)
      {
        //ROS_WARN_STREAM("target time " << o->time_);
        if (o->pose_.time_ > predecessor->pose_.time_)
        {
          predecessor = o;
          //ROS_ERROR_STREAM("target time faund" << o->time_);
        }
      }
      else
      {
        predecessor = o;
        //ROS_ERROR_STREAM("target time nptr" << o->time_);
      }
    }

    if (o->pose_.time_ > stamp)
    {
      if (sucessor != nullptr)
      {
        if (o->pose_.time_ < sucessor->pose_.time_)
        {
          sucessor = o;
        }
      }
      else
        sucessor = o;
    }
  }
  if (predecessor == nullptr)
  {
    ROS_WARN_STREAM("could not finde predecessor");
  }
  if (sucessor == nullptr)
  {
    ROS_WARN_STREAM("could not find sucessor");
  }
} //end func
//****************************************************************

shared_ptr<Node> LogicalGraph::FindCreateMatchingPose(const ros::Time &stamp)
{
  shared_ptr<Node> retval{nullptr};
  if (pose_object_container_.size() == 0)
  {
    retval = CreateFirstPoseObject(stamp);
    //ROS_INFO_STREAM(" First Pose ");
  }
  else if (latest_pose_object_->pose_.time_ <= stamp)
  {
    //ROS_INFO_STREAM(" Last Pose  eval");
    retval = CreateReturnLatestPoseObject(stamp);
    //ROS_INFO_STREAM(" Last Pose ");
    //CheckGraph();
  }
  else if (retval == nullptr)
  {
    //retval = FindKeyPose(cones, sensor_id);// keypose deactivated by this, evaluate if useful
    //ROS_INFO_STREAM(" Inbetween Node Eval ");
    retval = CreateReturnInbetweenNode(stamp);
    //ROS_INFO_STREAM(" Inbetween Node ");
    
  }
  return retval;
} //end func
//****************************************************************

Node LogicalGraph::CreateLatestPoseObject(const ros::Time &stamp)
{
  const auto next_key = GetNextKey();
  //ROS_INFO_STREAM("getting odo");
  const auto odo = vel_integrator_->OdoFromTo(latest_pose_object_->pose_.time_, stamp);
  //ROS_WARN_STREAM("odo x y t" << odo.x() << "  " << odo.y() << "  " << odo.theta());
  const auto integrated_pose = latest_pose_object_->pose_.pose_.compose(odo);
  //ROS_WARN_STREAM("integrated pose " << integrated_pose.x() << "  " << integrated_pose.y() << "  " << integrated_pose.theta());
  Pose pose(integrated_pose, stamp, next_key);
  //since it is the latest Pose, it  only has a predecessor edge
  Edge predecessor(latest_pose_object_->pose_.key_pose_,
                   next_key, latest_pose_object_->pose_.time_, odo, covar_odo_);
  std::vector<Edge> edges{predecessor};
  Node new_pose(edges, pose);
  new_pose.pose_.pos_covar_ = latest_pose_object_->pose_.pos_covar_;
  return new_pose;
} //end func
//****************************************************************

std::shared_ptr<Node> LogicalGraph::CreateReturnLatestPoseObject(const ros::Time &stamp)
{
  auto newnode = CreateLatestPoseObject(stamp);
  auto ptrnode = std::make_shared<Node>(newnode);
  pose_object_container_.push_back(ptrnode);
  pose_object_map.emplace(ptrnode->pose_.key_pose_, ptrnode);
  latest_pose_object_ = pose_object_container_.back();
  return latest_pose_object_;
} //end func
//****************************************************************

std::shared_ptr<Node> LogicalGraph::CreateFirstPoseObject(const ros::Time &stamp)
{
  auto key = GetNextKey();
  gtsam::Pose2 start_pose(params_.start_x, params_.start_y, params_.start_theta);
  Pose new_pose(start_pose, stamp, key);
  std::vector<Edge> empty;

  Node new_node(empty, new_pose);
  //ROS_INFO_STREAM("new_node  x " << new_node.pose_.pose_.x());
  auto ptrnode = std::make_shared<Node>(new_node);
  ptrnode->pose_.pos_covar_(0, 0) = 0.0; //todo make by param, first node covariance for matching
  ptrnode->pose_.pos_covar_(1, 1) = 0.0;
  ptrnode->pose_.pos_covar_(2, 2) = 0.0;
  pose_object_container_.push_back(ptrnode);
  pose_object_map.emplace(ptrnode->pose_.key_pose_, ptrnode);
  latest_pose_object_ = pose_object_container_.back();
  return latest_pose_object_;
} //end func
//****************************************************************

std::shared_ptr<Node> LogicalGraph::GetLatestNode()
{
  std::lock_guard<std::mutex> lk(graph_lock_);
  return latest_pose_object_;
} //end func
//****************************************************************

const std::vector<std::shared_ptr<const Node>> LogicalGraph::GetNewNodesAndEdges(const Graph_ID &id)
{
  std::lock_guard<std::mutex> lk(graph_lock_);
  std::vector<std::shared_ptr<const Node>> ret;
  //ROS_INFO_STREAM(" graphs id for getting poses : " << id.graph_id);
  for (auto elem : pose_object_container_)
  {

    if (elem->pose_.key_pose_ != gtsam::symbol('x', 0) 
     && elem->pose_.time_ > id.time_of_creation - id.max_lookback
     && elem->IsPoseUsed(id.graph_id) == false)
    {
      //ROS_INFO_STREAM("elem pos id " << elem->key_pose_ - gtsam::symbol ( 'x',0 ));
      ret.push_back(elem);
      elem->SetPosUsed(id.graph_id);
    }
  }
  return ret;
} //end func
//****************************************************************

const std::vector<std::shared_ptr<const Node>> LogicalGraph::GetMeasurements(const Graph_ID &id)
{
  std::lock_guard<std::mutex> lk(graph_lock_);
  std::vector<std::shared_ptr<const Node>> ret;
  for (auto &elem : pose_object_container_)
  {
    if (elem->pose_.key_pose_ != gtsam::symbol('x', 0)
     && elem->pose_.time_ > id.time_of_creation - id.max_lookback 
     && elem->IsMeasUsed(id.graph_id) == false && elem->IsPoseUsed(id.graph_id) == true //only if pose was already added/requestet by graph
     && elem->IsDataAsociated() == true)
    {
      ret.push_back(elem);
      elem->SetMeasUsed(id.graph_id);
    }
  }
  return ret;
} //end func
//****************************************************************


const std::vector<std::shared_ptr<const Node>> LogicalGraph::GetWindow(double window_size, const Graph_ID &id)
{
  std::lock_guard<std::mutex> lk(graph_lock_);
  std::vector<std::shared_ptr<const Node>> ret; //smart pointer ensure no memory errors, at worst results are not used
  //ROS_INFO_STREAM_COND(ESTIMATION::DEBUG_NUMPOSES, "number of poses in manager " << pose_object_container_.size());
  for (auto &elem : pose_object_container_)
  {
    if (elem->pose_.time_ > id.time_of_creation - id.max_lookback)
    {
      elem->SetMeasUsed(id.graph_id);
      elem->SetPosUsed(id.graph_id);
      ret.push_back(elem);
    }
  }
  return ret;
} //end func
//****************************************************************

void LogicalGraph::CleanUpGraph()
{
  //since it is fifo, oldest element will be first, so breaking
  //after this is totally fine
  ros::Duration d(params_.TTL);
  if (pose_object_container_.size() < 2)
  {
    return;
  }
  for (auto it = pose_object_container_.begin(); it != pose_object_container_.end(); ++it)
  {
    if (it.operator*()->pose_.time_ < ros::Time::now() - d)
    {
      //smart pointer ensure no memory errors, at worst results are not used
      //so even, if the poped pointer is used else where, no segfaults
      pose_object_map.erase(it.operator*()->pose_.key_pose_);
      it = pose_object_container_.erase(it);
    }
  }
} //end func
//****************************************************************

void LogicalGraph::UpdateNodes(const std::vector<Result> &results)
{
  std::lock_guard<std::mutex> lk(graph_lock_);
  shared_ptr<Node> last_local_pose{nullptr};
  for (const auto &result : results)
  {
    auto it = pose_object_map.find(result.key_pose_); //map is used for O(1) complexity in search
    if (it != pose_object_map.end())
    {
      it.operator*().second->pose_.pos_covar_(0, 0)  = 0.1;//result.pos_covar_(0, 0);
      it.operator*().second->pose_.pos_covar_(1, 1) = 0.1;//result.pos_covar_(0, 0);
      it.operator*().second->pose_.pos_covar_(2, 2) = 0.05;//result.pos_covar_(2, 2);
      it.operator*().second->pose_.pose_ = result.pose_;
      if (last_local_pose != nullptr)
      {
        if (last_local_pose->pose_.time_ < it.operator*().second->pose_.time_)
        {
          last_local_pose = it.operator*().second;
        }
      }
      else
      {
        last_local_pose = it.operator*().second;
      }
    }
  }
  //while calculating, new last pose came in. Update it according to odometry from last
  //calculated Position
  if (last_local_pose != nullptr && last_local_pose != latest_pose_object_)
  {
    //UpdateFromLasttoSource(last_local_pose);
  }

} //end func
//****************************************************************

void LogicalGraph::UpdateFromLasttoSource(const std::shared_ptr<Node> &source)
{
  // by sorting container to timespamps, only objects newer
  // than const std::shared_ptr<Node> &source are consired in the update Process
  SortObjectContainerAscendingTimestamps();
  const auto target_it = std::find(pose_object_container_.begin(), pose_object_container_.end(), source);
  if (target_it == pose_object_container_.end())
    return;
  auto no_poses_require_update = 0;
  for (auto it = pose_object_container_.rbegin(); it.base() != target_it; ++it)
    no_poses_require_update++;
  std::vector<Edge> edges;
  edges.reserve(no_poses_require_update * 2); //reserve for effiency
  for (auto it = pose_object_container_.rbegin(); it.base() != target_it; ++it)
  { //gather all edges
    for (const auto &edge : it->operator*().edges_)
    {
      edges.push_back(edge);
    }
  }
  for (auto it = target_it; it != pose_object_container_.end(); ++it)
  {
    for (const auto &edge : edges)
    {
      if (edge.pose_key_pre_ == it->operator*().pose_.key_pose_) //if current edge predecessor is target
      {
        auto pose_to_update = pose_object_map.find(edge.pose_key_suc_); //map for efficient search
        if (pose_to_update != pose_object_map.end())
        { //update from calulated pose + integrated odometry
          pose_to_update->second->pose_.pose_ = it->get()->pose_.pose_.compose(edge.odo_);
        }
      }
    }
  }
} //end func
//****************************************************************

void LogicalGraph::SortObjectContainerAscendingTimestamps()
{
  pose_object_container_.sort([](const std::shared_ptr<Node> &a, const std::shared_ptr<Node> &b) { return a->pose_.time_ < b->pose_.time_; });
} //end func
//****************************************************************

void LogicalGraph::CheckGraph()
{
  ROS_WARN_STREAM("Checking Graph on correctness");
  int end_cond = 0;
  SortObjectContainerAscendingTimestamps();
  auto first_pose = pose_object_container_.front();
  auto iterating_pose = pose_object_container_.back();
  while (iterating_pose != first_pose)
  {
    size_t val_key;
    for (const auto &edge : iterating_pose->edges_)
    {
      if (edge.pose_key_pre_ != iterating_pose->pose_.key_pose_)
      {
        val_key = edge.pose_key_pre_;
      }
    }
    iterating_pose = pose_object_map.at(val_key);
    if (end_cond++ > 10000)
    {
      ROS_ERROR_STREAM(" Graph is broken");
      ROS_BREAK();
    }
  }
} // end func
//****************************************************************

}//end ns estimation