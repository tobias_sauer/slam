//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "frontend.hpp"
namespace estimation{

Frontend::Frontend ( ESTIMATION::Frontend_Params& params )
{
  params_ = params  ;
  if(params_.gps_activated == true)
  {
    ROS_WARN_STREAM("GPS ACTIVATED");
    gps_node_ = std::make_unique<GPS>(params_.gpsparams_, [this](gtsam::Pose2 a, ros::Time b, Eigen::Matrix3f c) { CbGPS(a, b, c); });
  }


  if ( params.mode == ESTIMATION::MODE::LOCALISATION ) //in slam mode, map is built  by ourself, so no map callbacks req
  {
      MapListener_ =  std::make_unique<MapListener>(params.map_input_params_
                                        ,[this](d4_comm_pkg::RawMap a){CBMap(a);}  ); //fptr
  }
  //init inputs
  for ( ESTIMATION::Sensor_Params &s : params.sensors )
    {
      cone_inputs_.push_back( std::make_unique<Cone_Listener> ( s //sensor params
                              , [this](d4_comm_pkg::ConeArray a,int b){CBCones(a,b);} ) ); //fptr
    }
  vel_integrator_ = std::make_shared<VelocityIntegrator>(params.velocity_params);

  if(params.wait_for_res == true)
  {
    init_states_.received_res_ = false;
        ROS_WARN_STREAM(" created res listener");
    res_listener_ = std::make_unique<RESLister>(1
                                                ,[this](){ CBRes();} 
                                                , params.nh_);
  }  
  data_asoc_ = std::make_unique<NN_DA>(params.da_params);
  ros::Time::waitForValid();
  logical_graph_ = std::make_unique<LogicalGraph>(vel_integrator_, params);
  init_states_.initalized_frontend = true;
  matching_viz_ = std::make_unique<MatchingViz>(params.nh_);
  if(params_.startup_delay != 0)
  { //startup deley for e.g. gps
   // timer_ = params.nh_->createTimer(
   //     ros::Duration(params_.startup_delay), [this](const ros::TimerEvent & a) { CbTimer(a); }, true);
  }
  ROS_WARN_STREAM("estimation : Initialized frontend");
}//end constructor 
//****************************************************************


void Frontend::CBCones ( d4_comm_pkg::ConeArray cones, int sensor_id )
{
  if(Initialized() == false || cones.cones.size() == 0) return;// check if all is in the right states, without cones data, nothing gets triggered
  ROS_INFO_STREAM(" Cone Measurement for Sensor " << sensor_id << " came in at " << cones.header.stamp);
  auto matchingnode = logical_graph_->AddMeasurementToGraph(cones.header.stamp);
  ROS_INFO_STREAM("added node");
  if(matchingnode != nullptr)
  {
    matchingnode->sensors_params = GetParamsFromID(sensor_id);
    AddProcessMeasurements ( matchingnode,cones );
    EvaluateTrigger(sensor_id);
  }
  else
  {
    ROS_FATAL_STREAM("Could not Sort Pose into Graph, something got Wrong");
    ROS_BREAK();
  }
}//end func
//****************************************************************

void Frontend::CbGPS(gtsam::Pose2 gps_pose, ros::Time stamp, Eigen::Matrix3f cov)
{
  if (Initialized() == false )
    return; // check if all is in the right states, without cones data, nothing gets triggered
  ROS_INFO_STREAM(" Gps Measurement came in at " << stamp);
  auto matchingnode = logical_graph_->AddMeasurementToGraph(stamp);
  matchingnode->SetGpsPose(gps_pose,cov);
  EvaluateTrigger(0);
}

ESTIMATION::Sensor_Params Frontend::GetParamsFromID(int sensor_id) const
{
  for(auto &s : params_.sensors)
    if(s.id == sensor_id) return s;
}//end func
//****************************************************************

void Frontend::CbTimer(const ros::TimerEvent &e)
{
  init_states_.timer = true;
} //end func
//****************************************************************

d4_comm_pkg::PoseVel Frontend::GetActualState()
{
  auto latest_pose_object_  = logical_graph_->GetLatestNode();
  d4_comm_pkg::PoseVel retval;
  auto latest_pose = latest_pose_object_->pose_.pose_;
  retval.Pose.pos_x = latest_pose.x();
  retval.Pose.pos_y = latest_pose.y();
  retval.Pose.pos_theta = latest_pose.theta();
  if(params_.compensate_delay == true)
  {
    auto odo = vel_integrator_->OdoFromTo(latest_pose_object_->pose_.time_,ros::Time::now());
    latest_pose = latest_pose.compose(odo);
    retval.Pose.pos_x = latest_pose.x();
    retval.Pose.pos_y = latest_pose.y();
    retval.Pose.pos_theta = latest_pose.theta();
    //ROS_INFO_STREAM("Latest Pose odo Fill up " << odo.x() << " " << odo.y() << " " <<  odo.theta());
  }
  const auto velocity = vel_integrator_->GetSpeed();
  retval.State = velocity.State;
  return retval;
}//end func
//****************************************************************

void Frontend::UpdateNodes(const std::vector<Result> &results)
{
  logical_graph_->UpdateNodes(results);
}//end func
//****************************************************************

const std::vector<std::shared_ptr<const Node>> Frontend::GetNewNodesAndEdges(const Graph_ID &id)
{
  return logical_graph_->GetNewNodesAndEdges(id);
} //end func
//****************************************************************

void Frontend::SetLocalizationMode()
{
  params_.mode = ESTIMATION::MODE::LOCALISATION;
} //end func
//****************************************************************

map<uint64_t, Measurement> Frontend::GetNewLandmarks()
{
  return data_asoc_->GetNewLandmarks();
} //end func
//****************************************************************

const std::vector<std::shared_ptr<const Node>> Frontend::GetMeasurements(const Graph_ID &id)
{
  return logical_graph_->GetMeasurements(id);
} //end func
//****************************************************************

void Frontend::AddProcessMeasurements (std::shared_ptr<Node> &p,const d4_comm_pkg::ConeArray &cones )
{
  if(cones.cones.size() == 0)
  {
    ROS_WARN_STREAM("Measurement without Cones!");
    return;
  }
  std::vector<Measurement> meas;
  meas.reserve(cones.cones.size());
  for (const d4_comm_pkg::Cone &c : cones.cones)
  {
    Measurement m(c);
    m.meas_covar_(1, 0) = 0;
    m.meas_covar_(0, 1) = 0;
    m.meas_covar_(0, 0) = c.covariance.at(1);
    m.meas_covar_(1, 1) = c.covariance.at(0);
    if (params_.plattform == ESTIMATION::PLATTFORM::D4 ||
        params_.plattform == ESTIMATION::PLATTFORM::MARINA)
        {
          m.color = c.cone_type;
        }
      meas.emplace_back(m);
  }
  p->AddMeasurements(meas);  
  if(params_.mode == ESTIMATION::MODE::LOCALISATION)
  {
    auto result = data_asoc_->NNDataAssociation(*p);
    p->UpdateMeasurementKeys(result);
  }
  else
  {
    ROS_INFO_STREAM("mapping da");
    auto result = data_asoc_->MappingDataAssociation(*p);
    p->UpdateMeasurementKeys(result);
  }
  matching_viz_->VizualizeMatching(p,map_);
  ROS_INFO_STREAM("after viz");
}//end func
//****************************************************************

map< uint64_t, Measurement > Frontend::GetMap()
{
  ROS_INFO_STREAM ( "BACKEND REQUETS MAP" );
  if(map_.size() == 0)
  {
    signal_.Take();
  }
  ROS_INFO_STREAM ( "FRONTEND GAVE MAP TO BACKEND" );
  return map_;
}//end func
//****************************************************************

void Frontend::UpdateMap ( std::map< uint64_t, Measurement > map )
{
  map_ = map;
  data_asoc_->Update_Map ( map );
}//end func
//****************************************************************

void Frontend::GetTrigger()
{
  ROS_INFO_STREAM("waiiting trig");
  signal_slam_.Take();
}//end func
//****************************************************************

void Frontend::EvaluateTrigger(int sensor_id)
{
  signal_slam_.Give();
}//end func
//****************************************************************


void Frontend::CBRes()
{
  ROS_WARN_STREAM("res registered in frontend");
  init_states_.received_res_ = true;
}

bool Frontend::Initialized() const
{
  auto initialized = false;
  if(init_states_.initalized_frontend == false){return false;}
  switch (params_.mode)
  {
  case ESTIMATION::MODE::LOCALISATION :
    if(params_.wait_for_res == true && init_states_.received_res_ == true
      && init_states_.received_map_ == true)
      { 
        initialized = true;
        return initialized;
      }
    else if(params_.wait_for_res == false && init_states_.received_map_ == true)
      { 
        initialized = true;
        return initialized;
      }
    else
      { 
        initialized = false;
        return initialized;
      }
    break;

  case ESTIMATION::MODE::MAPPING:  
    if(params_.wait_for_res == true && init_states_.received_res_ == true)
      { 
        initialized = true;
        return initialized;
      }
    else if (params_.wait_for_res == false )
      { 
        initialized = true;
        return initialized;
      }
    else 
      { 
        initialized = false;
        return initialized;
      }
    break;
  default:
    ROS_ERROR_STREAM("No Mode Defines in Frontend");
    ROS_BREAK();
  }
  return initialized;
}//end func
//****************************************************************


const std::vector<std::shared_ptr<const Node>> Frontend::GetWindows(double window_size, const Graph_ID &id)
{
  //ROS_INFO_STREAM("Getting Window for graph id " << id.graph_id);
  auto ret = logical_graph_->GetWindow(window_size, id);
  if (ret.size() <= 10) //wait for startup
  {
    signal_slam_.Take();
    ret = GetWindows(window_size, id);
  }
  //ROS_INFO_STREAM_COND(ESTIMATION::DEBUG_NUMPOSES, "number of poses in window " << ret.size());
  return ret;
} //end func
//****************************************************************

void Frontend::CBMap(const d4_comm_pkg::RawMap &map)
{
  size_t i = 0;
  for (const d4_comm_pkg::Cone &cony : map.cones)
  {
    Measurement m(cony.x, cony.y);
    m.meas_covar_.setZero();
    m.meas_covar_(0, 0) = cony.covariance.at(0);
    if (cony.covariance.size() == 2) //sometimes cov is given as diagonal with 2 entries
    {
      m.meas_covar_(1, 1) = cony.covariance.at(1);
    }
    else
    {
      m.meas_covar_(1, 1) = cony.covariance.at(3);
    }
    map_.insert({gtsam::symbol('l', i++), m});
  }
  init_states_.received_map_ = true;
  data_asoc_->Update_Map(map_);
  signal_.Give();
  ROS_INFO_STREAM("FRONTEND RECEIVED MAP, SIGNAL GIVEN");
} //end func
  //****************************************************************
}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
