//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "slam.hpp"

namespace estimation{

namespace CONCURRENT_STATES
{
constexpr int START_INIT_FIRST = 1;
constexpr int FIRST_RUNNING = 2;
constexpr int WAIT_SECOND = 3;
constexpr int SECOND_RUNNING = 4;
constexpr int WAIT_FIRST = 5;
};


SLAM::SLAM()
{
  ros::NodeHandle nh;
  ParamLoader loader;
  loader.LoadParams ( params_,&nh );
  if ( params_.plattform != PLATTFORM::SIM ) //since in sim TF is given
    {
      tf_pub_ = std::make_unique<TF_Pub> ( params_.tf_params  );
    }
  pub_ =  std::make_unique<Pos_Pub>( params_.pub_params );
  frontend_ = std::make_unique<Frontend> ( params_.frontend_params );
  viz_ = std::make_unique<Visualization> ( &nh  );
  std::thread t1 ( &SLAM::MainThread,this );
  ros::MultiThreadedSpinner spinner ( 4 );
  spinner.spin();
  t1.join();
}//endfunc
//****************************************************************

void SLAM::MainThread()
{
  if ( params_.mode == MODE::LOCALISATION ) //Init
    {
      LocalizationInit();
    }
    else
    {
      ROS_INFO_STREAM("initing mao");
      MappingInit();
      ROS_INFO_STREAM("finised map");
    }
  for ( ;; ) 
    {
      MainLoop();
    } //End main loop
}//endfunc
//****************************************************************

void SLAM::MainLoop()
{
  frontend_->GetTrigger();
  ROS_INFO_STREAM_ONCE ( "Got Triggered" );
  const auto now = ros::WallTime::now();
  switch ( params_.mode )
    {
      case MODE::LOCALISATION:
      {
        const auto time_now = ros::WallTime::now();
        Localization();
        ROS_INFO_STREAM("Calc Time Localization : " << ros::WallTime::now().toSec() - time_now.toSec());
      }//End Localization switch case
      break;
      case MODE::MAPPING:
      {
        Mapping();
      }
      break;
      default:
        ROS_ERROR_STREAM ( "Nothing Selected" );
        ROS_BREAK();
        break;
    }//End mode Switch Case
    HandleOutput();
}//endfunc
//****************************************************************

void SLAM::Mapping()
{

  if (skip_mapping_++ >= 0)
  {
    skip_mapping_ = 0;
      const auto actual_state = frontend_->GetActualState();
    const auto is_loop_closed = IsLoopClosed(actual_state);
    const auto newlandmarks = frontend_->GetNewLandmarks();
    local_graph_.AddNewLandmarks(newlandmarks);
    if (is_loop_closed == false)
    {
      IncrementalLocalization(local_graph_, id_local_graph_, init_local_graph_);
      const auto updated_map = local_graph_.GetMap();
      frontend_->UpdateMap(updated_map);
      pub_->PublishMap(updated_map,false);
    }
    else
    {
      CloseLoop(actual_state);
    }
  }
} //endfunc
//****************************************************************

bool SLAM::IsLoopClosed(const d4_comm_pkg::PoseVel &state)
{
  distance_travelled_ += std::hypot(last_pose.x() - state.Pose.pos_x, last_pose.y() - state.Pose.pos_y );
  last_pose = gtsam::Pose2(state.Pose.pos_x, state.Pose.pos_y, state.Pose.pos_theta);
  const auto distance_from_start =  std::hypot(state.Pose.pos_x, state.Pose.pos_y);
  if (distance_travelled_ > 100.0 && distance_from_start < 5.0f)
  {
    return true;
  }
  else
  {
    return false;
  }
  
}

void SLAM::CloseLoop(const d4_comm_pkg::PoseVel &state)
{
  local_graph_.SetNextCycleLoopClosing();
  IncrementalLocalization(local_graph_, id_local_graph_, init_local_graph_);
  const auto updated_map = local_graph_.GetMap();
  pub_->PublishMap(updated_map,true);
  frontend_->UpdateMap(updated_map);
  frontend_->SetLocalizationMode();
  params_.mode = MODE::LOCALISATION;
  const auto actual_state = frontend_->GetActualState();
  params_.frontend_params.start_x = actual_state.Pose.pos_x;
  params_.frontend_params.start_y = actual_state.Pose.pos_y;
  params_.frontend_params.start_theta = actual_state.Pose.pos_theta;
  LocalizationInit();
}

void SLAM::MappingInit()
{

}//endfunc
//****************************************************************

void SLAM::LocalizationInit()
{

  auto map = frontend_->GetMap();
  init_local_graph_ = CreateLocalizationInit ( map,
                          gtsam::Pose2 ( params_.frontend_params.start_x
                                         ,params_.frontend_params.start_y
                                         ,params_.frontend_params.start_theta ) );
  id_local_graph_ = local_graph_.CreateNewGraph ( init_local_graph_,1 );
  id_local_graph_two_.graph_id = 2;
  id_local_graph_two_.time_of_creation = ros::Time::now();
  concurrent_state_ = CONCURRENT_STATES::START_INIT_FIRST;
  pub_->PublishMap ( map,true );
}//endfunc
//****************************************************************
static int skip=0;
void SLAM::Localization()
{
  ROS_INFO_STREAM_ONCE ( "Localization" );
  switch ( params_.graph_params.graph_mode_localization )
    {
    case GRAPHMODE_LOCALIZATION::STANDARD:
    {
      ROS_INFO_STREAM_ONCE ( "Standard Localization" );
      if(skip++)
      {
        IncrementalLocalization ( local_graph_,id_local_graph_,init_local_graph_ );
      }
      else
      {
        ROS_INFO_STREAM_ONCE("Windowed Localization");
        const auto changed_poses = WindowedLocalization(local_graph_, id_local_graph_, init_local_graph_);
        ROS_INFO_STREAM_ONCE("Updating Poses");
        const auto results = local_graph_.UpdatePoseObjects(changed_poses);
        frontend_->UpdateNodes(results);
      }
    }
    break;
    case GRAPHMODE_LOCALIZATION::WINDOWED:
    {
      ROS_INFO_STREAM_ONCE ( "Windowed Localization" );
      const auto changed_poses = WindowedLocalization ( local_graph_,id_local_graph_,init_local_graph_ );
      ROS_INFO_STREAM_ONCE ( "Updating Poses" );
      const auto results = local_graph_.UpdatePoseObjects ( changed_poses );
      frontend_->UpdateNodes(results);
    }
    break;
    case GRAPHMODE_LOCALIZATION::CON_WINDOWS:
    {
      ROS_INFO_STREAM_ONCE ( "Concurrent Localization" );
      ConcurrentLocalization();
    }
    break;
    default:
      ROS_ERROR_STREAM ( "No Localization Mode selected" );
      ROS_BREAK();
      break;
    }
}//endfunc
//****************************************************************

void SLAM::HandleOutput()
{
  auto out = frontend_->GetActualState();
  if ( params_.plattform != PLATTFORM::SIM ) //Update TF
    {
      gtsam::Pose2 tf_pos(out.Pose.pos_x, out.Pose.pos_y, out.Pose.pos_theta);
      tf_pub_->Update_TF(tf_pos);
    }
  //Publish Pose
  pub_->Publish ( out );
  ROS_INFO_STREAM(" x " << out.Pose.pos_x << " y " << out.Pose.pos_y);
  if(params_.mode == MODE::MAPPING) //Publish (new) Map
  {
    //pub_->PublishMap(init_local_graph_.map_, fa);
  }
  else
  {
    pub_->PublishMap(init_local_graph_.map_,true); //just so rviz gets this ...
  }
}//endfunc
//****************************************************************

void SLAM::ConcurrentLocalization()
{
  ROS_INFO_STREAM_ONCE ( "Concurrent Mode" );
  switch ( concurrent_state_ )
    {
      case CONCURRENT_STATES::START_INIT_FIRST:
      {

        ConcurrentInit(local_graph_,id_local_graph_,init_local_graph_);
      }
      break;
      case CONCURRENT_STATES::FIRST_RUNNING:
      {
        ConcurrentRun(local_graph_,local_graph_two_,id_local_graph_,id_local_graph_two_,init_local_graph_);
      }
      break;
      case CONCURRENT_STATES::WAIT_SECOND:
      {
        ConcurrentWait(local_graph_,local_graph_two_,id_local_graph_,id_local_graph_two_,init_local_graph_);
      }
      break;
      case CONCURRENT_STATES::SECOND_RUNNING:
      {
        ConcurrentRun(local_graph_two_,local_graph_,id_local_graph_two_,id_local_graph_,init_local_graph_);
      }
      break;
      case CONCURRENT_STATES::WAIT_FIRST:
      {
        ConcurrentWait(local_graph_two_,local_graph_,id_local_graph_two_,id_local_graph_,init_local_graph_);
      }
      break;
      default:
        ROS_ERROR_STREAM("No State in Concorruent Graphmode");
        ROS_BREAK();
    }
}//endfunc
//****************************************************************

void SLAM::ConcurrentInit ( LocalGraph &run_graph,Graph_ID &run_id,Graph_Init &init )
{
  Graph_ID temp_id = run_id;
  temp_id.max_lookback.fromSec(100);
  const auto changed_poses = WindowedLocalization(run_graph, temp_id, init);
  const auto results = local_graph_.UpdatePoseObjects ( changed_poses );
  frontend_->UpdateNodes(results);
  local_graph_.ClearValues();
  concurrent_state_ = CONCURRENT_STATES::FIRST_RUNNING;
}//endfunc
//****************************************************************

void SLAM::ConcurrentRun ( LocalGraph &run_graph,LocalGraph &next_graph,Graph_ID &run_id,Graph_ID &next_id,Graph_Init &init )
{
      IncrementalLocalization ( run_graph,run_id,init );
      ros::Duration d(params_.graph_params.graph_con_lifetime);
      ros::Duration d_h(params_.graph_params.graph_con_lifetime/2.0);
      //ROS_INFO_STREAM("RUN Stamp : " <<  run_id.graph_id << " t " << run_id.time_of_creation);
      //ROS_INFO_STREAM("next Stamp : " <<  next_id.graph_id << " t " << next_id.time_of_creation);
      
      if ( (run_id.time_of_creation + d) < ros::Time::now() )
        {
          next_id.time_of_creation = ros::Time::now();
          next_id.max_lookback = d;
          next_id.graph_id += 2;
          auto g2_poses =  frontend_->GetWindows ( params_.graph_params.graph_con_lifetime,next_id );
          next_graph.AddInitWindow ( g2_poses,init,next_id );
          ROS_WARN_STREAM("beffore ud");
          next_graph.ComputeUpdateThreaded ( true );
          ROS_WARN_STREAM("after ud");
          if(run_id.graph_id%2 == 0)
          {
             ROS_INFO_STREAM("Change to CONCURRENT_STATES::WAIT_FIRST");
            concurrent_state_ = CONCURRENT_STATES::WAIT_FIRST;
          }
          else
          {
            ROS_INFO_STREAM("Change to CONCURRENT_STATES::WAIT_SECOND");
            concurrent_state_ = CONCURRENT_STATES::WAIT_SECOND;
          }
        }
}//endfunc
//****************************************************************

void SLAM::ConcurrentWait( LocalGraph &run_graph,LocalGraph &next_graph,Graph_ID &run_id,Graph_ID &next_id,Graph_Init &init)
{
      IncrementalLocalization ( run_graph,run_id,init );
      if ( next_graph.ready_ == true )
        {
          next_graph.ClearValues();
          if(run_id.graph_id%2 == 0)
          {
            ROS_INFO_STREAM("Change to CONCURRENT_STATES::SECOND_RUNNING");
            concurrent_state_ = CONCURRENT_STATES::FIRST_RUNNING;
          }
          else
          {
            ROS_INFO_STREAM("Change to CONCURRENT_STATES::FIRST_RUNNING");
            concurrent_state_ = CONCURRENT_STATES::SECOND_RUNNING;
          }
        }
}//endfunc
//****************************************************************

const vector<std::shared_ptr<const Node>> SLAM::WindowedLocalization ( LocalGraph &graph,const Graph_ID &id,const Graph_Init &init )
{
  auto poses = frontend_->GetWindows ( params_.window_size,id );
  graph.AddInitWindow ( poses,init,id );
  ROS_WARN_STREAM("after ud windowed ud");
  graph.Update();
   ROS_WARN_STREAM("after update");
  std::vector<std::shared_ptr<const Node>> ret_poses;
  auto skip=0; //skip first element
  for ( const auto &elem : poses )
    {
      if ( skip++ )
        {
          ret_poses.push_back ( elem );
        }
    }
  ROS_WARN_STREAM("Returning ret poses from window");  
  return ret_poses;
}//endfunc
//****************************************************************

void SLAM::IncrementalLocalization ( LocalGraph &graph,Graph_ID &id,Graph_Init &init )
{
  auto t_c = ros::WallTime::now();
  const auto poses =  frontend_->GetNewNodesAndEdges ( id );
  ROS_INFO_STREAM("Adding Pose id : " << id.graph_id << " got " << poses.size() << " new poses ");
  if(poses.size()!= 0) graph.AddPoses ( poses );
  const auto measurements = frontend_->GetMeasurements ( id );
  if(measurements.size()!= 0) graph.AddMeasurements ( measurements );
     ROS_INFO_STREAM(" time getting tings  " << ros::WallTime::now() -t_c);
  graph.Update();
  const auto results = graph.UpdatePoseObjects ( poses,measurements );
  frontend_->UpdateNodes(results);
  graph.ClearValues();
}//endfunc
//****************************************************************

Graph_Init SLAM::CreateLocalizationInit ( std::map< uint64_t, Measurement >& map,gtsam::Pose2 start_pose )
{
  Graph_Init init;
  init.map_ = map;
  return init;
}//endfunc
//****************************************************************

Graph_Init SLAM::CreateMappingInit()
{
  auto map = frontend_->GetMap();
  init_local_graph_ = CreateLocalizationInit(map,
                                             gtsam::Pose2(params_.frontend_params.start_x, params_.frontend_params.start_y, params_.frontend_params.start_theta));
  id_local_graph_ = local_graph_.CreateNewGraph(init_local_graph_, 1);
  id_local_graph_two_.graph_id = 2;
  id_local_graph_two_.time_of_creation = ros::Time::now();
  concurrent_state_ = CONCURRENT_STATES::START_INIT_FIRST;
  pub_->PublishMap(map, true);
}

}//end namespace estimation
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
