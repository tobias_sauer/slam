//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include "local_graph.hpp"


namespace NM = gtsam::noiseModel;
namespace estimation{
void LocalGraph::Update()
{
  isam.update ( local_graph_,local_values_ );
}//endfunc
//****************************************************************

void LocalGraph::AddPoses ( const std::vector<std::shared_ptr<const Node> >  &nodes )
{
  if(initalised_ == false)
  {
    auto begin = nodes.front();
    InitGraph(local_map_,*begin);
  }

  for ( auto &elem  : nodes )
    {
      AddPoseToGraph ( elem.operator*() );
    }
  if (close_loop_)
  {
    close_loop_ == false;
    NM::Diagonal::shared_ptr noise_odo = gtsam::noiseModel::Diagonal::Sigmas(gtsam::Vector3(1, 1, 0.5));
    const auto last = nodes.back();
    const auto odo = last->pose_.pose_.between(gtsam::Pose2(0,0,0));
    ROS_ERROR_STREAM("loop closing distances : " << odo.x() << odo.y());
    local_graph_.add(gtsam::BetweenFactor<gtsam::Pose2>(last->pose_.key_pose_, first_key, odo, noise_odo));
  }
}//endfunc
//****************************************************************

void LocalGraph::AddMeasurements ( const std::vector<std::shared_ptr<const Node> >  &nodes  )
{
  for ( auto &elem : nodes )
    {
      AddMeasurementsToGraph ( elem.operator*() );
    }
}//endfunc
//****************************************************************

const std::vector< Result> LocalGraph::UpdatePoseObjects (const std::vector< std::shared_ptr<const Node> >&nodes 
                                  , const std::vector< std::shared_ptr<const Node> >&measurements  )
{
  std::vector< Result> retval; 
  retval.reserve(nodes.size()+ measurements.size());
  for ( auto &elem : nodes )
    {
      const auto result = isam.calculateEstimate<gtsam::Pose2>(elem->pose_.key_pose_);
      Pose retob(result, elem->pose_.time_, elem->pose_.key_pose_);
      retob.pos_covar_ = isam.marginalCovariance(elem->pose_.key_pose_).cast<float>();
      //ROS_WARN_STREAM(" Pos covar by isam : " << retob.pos_covar_);
      retval.emplace_back(retob);
    }
  for ( auto &elem : measurements )
    {
      const auto result = isam.calculateEstimate<gtsam::Pose2>(elem->pose_.key_pose_);
      Pose retob(result, elem->pose_.time_, elem->pose_.key_pose_);
      retob.pos_covar_ = isam.marginalCovariance(elem->pose_.key_pose_).cast<float>();
    
      retval.emplace_back(retob);
    }
    if(retval.size() != 0)
    {
      ROS_WARN_STREAM(" Pos covar by isam : " << retval.back().pos_covar_);
    }
    return retval;
}//endfunc
//****************************************************************

const std::vector< Result> LocalGraph::UpdatePoseObjects ( const std::vector< std::shared_ptr<const Node> >&nodes  )
{
  std::vector< Result> retval; 
  retval.reserve(nodes.size());
  for ( auto &elem : nodes )
    {
      
      const auto result = isam.calculateEstimate<gtsam::Pose2> ( elem->pose_.key_pose_ );
      Pose retob(result, elem->pose_.time_, elem->pose_.key_pose_);
      retob.pos_covar_ = isam.marginalCovariance(elem->pose_.key_pose_).cast<float>();
      //ROS_WARN_STREAM(" Pos covar by isam : " << retob.pos_covar_);
      retval.emplace_back(retob);
    }
  return retval;
}//endfunc
//****************************************************************

void LocalGraph::ClearValues()
{
  local_graph_.resize ( 0 );
  local_values_.clear();
}//endfunc
//****************************************************************

void LocalGraph::AddNewLandmarks ( map< uint64_t, Measurement> input )
{
  for ( const auto &elem : input )
    {
      gtsam::noiseModel::Diagonal::shared_ptr measurementNoise = gtsam::noiseModel::Diagonal::Sigmas(gtsam::Vector2(elem.second.meas_covar_(0, 0), elem.second.meas_covar_(1, 1)));
      gtsam::Point2 p(elem.second.x_, elem.second.y_);
      //local_graph_.emplace_shared<gtsam::PriorFactor<gtsam::Point2>>(elem.first, p, measurementNoise);
      local_values_.insert ( elem.first,p );
      Measurement target(elem.second.x_,elem.second.y_);
      local_map_.insert ( {elem.first,target} );
    }
}//endfunc
//****************************************************************

void LocalGraph::InitGraph (const map< uint64_t, Measurement >& input,const Node &node )
{
  isam = gtsam::ISAM2(); //reinitialize the isam 2
  ClearValues(); //and the working vars
  added_poses.clear();
  gtsam::Vector priorSigmas = gtsam::Vector3 ( 0.0,0.0,0.0 ); //set covar for first pos
  const NM::Base::shared_ptr  priorNoise = NM::Diagonal::Sigmas ( priorSigmas );
  gtsam::PriorFactor<gtsam::Pose2> prior_pose ( node.pose_.key_pose_, node.pose_.pose_, priorNoise );
  local_graph_.add ( prior_pose ); //and at is as first pose to graph + values
  local_values_.insert ( node.pose_.key_pose_ ,node.pose_.pose_ );
  first_key = node.pose_.key_pose_;
  initalised_ = true;
  ROS_WARN_STREAM(" init : added pose no : " << node.pose_.key_pose_);
  added_poses.emplace(node.pose_.key_pose_);
  //init map
  AddMapToGraph(input);
  local_map_ = input;
  //initialise
  isam.update ( local_graph_, local_values_ );
  ClearValues();
}//endfunc
//****************************************************************

Eigen::Matrix3f LocalGraph::GetPoseCovar ( Node& obj )
{
  Eigen::Matrix3d dretval = isam.marginalCovariance ( obj.pose_.key_pose_ );
  Eigen::Matrix3f retval;
  retval = dretval.cast<float>();
  return retval;
}//endfunc
//****************************************************************

Graph_Init LocalGraph::GetNewGrapInit()
{
  Graph_Init retval;
  retval.map_  = local_map_;
  retval.pos_  = last_pose;
  retval.pos_covar_ ( 0,0 ) = 0.1f;
  retval.pos_covar_ ( 1,1 ) = 0.1f;
  retval.pos_covar_ ( 2,2 ) = 0.1f;
  return retval;
}//endfunc
//****************************************************************

map< uint64_t, Measurement > LocalGraph::GetMap()
{
  for(auto &m : local_map_)
  {
    auto result = isam.calculateEstimate<gtsam::Point2>(m.first);
    m.second.x_ = result.x();
    m.second.y_ = result.y();
  }
  return local_map_;
}//endfunc
//****************************************************************

void LocalGraph::AddInitWindow (const std::vector<std::shared_ptr<const Node> > &nodes,const Graph_Init& init,const Graph_ID &id )
{
  //first pose is used as initialization Pose
  my_id = id;
  initalised_ = false;
  auto init_o  =  nodes.at ( 0 );
  InitGraph ( init.map_,*init_o);
  //add poses and measurements , skip the first element, since it is used for pose init
  auto skip =0;
  for ( auto &elem : nodes )
    {
      if ( skip++ )
        {
          AddPoseToGraph ( elem.operator*() );
          AddMeasurementsToGraph ( elem.operator*() );
        }
    }
}//endfunc
//****************************************************************

void LocalGraph::AddMeasurementsToGraph (const Node& o )
{
  const auto meas = o.GetMeasuremnts();
  if( o.IsDataAsociated() == false)
  {
    ROS_WARN_STREAM("No associated Data in for adding to Gtsam");
  }
  int added_to_graph = 0;
  for ( const auto &p : meas)
    {
      const auto present_in_map = local_map_.find(p.key_meas_);
      if (p.key_meas_ != MAGIC::NUMBER &&  present_in_map != std::end(local_map_))
      {
        const auto bearing = atan2(p.y_, p.x_);
        const auto range = std::hypot(p.x_, p.y_);
        //auto gaussian = NM::Isotropic::Sigma(2, 1); // non-robust
        //auto tukey = NM::Robust::Create(NM::mEstimator::Tukey::Create(15), gaussian);
        NM::Diagonal::shared_ptr noise_meas_K = gtsam::noiseModel::Diagonal::Sigmas(gtsam::Vector2(p.meas_covar_(1, 1), p.meas_covar_(0, 0)));
        local_graph_.add(gtsam::BearingRangeFactor<gtsam::Pose2, gtsam::Point2>(o.pose_.key_pose_, p.key_meas_,
                                                                                gtsam::Rot2(bearing), range, noise_meas_K));
        added_to_graph++;
      }
    }
  //ROS_WARN_STREAM("Added " << added_to_graph << " measurments with key out of " << meas.size());
}//endfunc
//****************************************************************

void LocalGraph::AddPoseToGraph (const Node& o )
{
  //covar x,y in cm, theta in rad

  for(const auto &f : o.edges_)
  {
    
    if(f.pred_stamp_> my_id.time_of_creation-my_id.max_lookback)
    {
      ROS_WARN_STREAM(" edge cov" << f.covar_);
      NM::Diagonal::shared_ptr noise_odo = gtsam::noiseModel::Diagonal::Sigmas(gtsam::Vector3(f.covar_(0, 0), f.covar_(1, 1), f.covar_(2, 2)));
      local_graph_.add ( gtsam::BetweenFactor<gtsam::Pose2> ( f.pose_key_pre_, f.pose_key_suc_, f.odo_,noise_odo ) );
    }
  }
  auto found = added_poses.find(o.pose_.key_pose_);
  if(found == std::end(added_poses))
  {
    ROS_WARN_STREAM(" add poses : added pose no : " << o.pose_.key_pose_);
    local_values_.insert ( o.pose_.key_pose_, o.pose_.pose_ );
    added_poses.emplace(o.pose_.key_pose_);
    if(o.IsGps())
    {
      const auto cov = o.GetGPSCovar();
      NM::Diagonal::shared_ptr noise_gps = gtsam::noiseModel::Diagonal::Sigmas(gtsam::Vector3(cov(0, 0), cov(1, 1), cov(2, 2)));
      local_graph_.emplace_shared<gtsam::PriorFactor<gtsam::Pose2>>(o.pose_.key_pose_, o.pose_.pose_, noise_gps);
    }
  }
}//endfunc
//****************************************************************

void LocalGraph::AddMapToGraph (const map< uint64_t, Measurement >& input )
{
  for (const auto &elem : input )
    {
      gtsam::Point2 p ( elem.second.x_, elem.second.y_ );
      gtsam::noiseModel::Diagonal::shared_ptr measurementNoise = gtsam::noiseModel::Diagonal::Sigmas ( gtsam::Vector2 ( elem.second.meas_covar_(0,0)
                                                                                        ,elem.second.meas_covar_(1,1) ) );
      local_graph_.emplace_shared<gtsam::PriorFactor<gtsam::Point2>> ( elem.first,p,measurementNoise );
      local_values_.insert ( elem.first,p );
    }
}//endfunc
//****************************************************************

void LocalGraph::ComputeUpdateThreaded ( bool detached )
{
  std::thread t1 ( &LocalGraph::ComputationTread,this );
  ready_ = false;
  ROS_INFO_STREAM("UPDATING ");
  if ( detached == true )
    {
      ROS_INFO_STREAM(" THREADED");
      t1.detach();
    }
  else
    {
      t1.join();
    }
}//endfunc
//****************************************************************

void LocalGraph::ComputationTread()
{
  isam.update ( local_graph_,local_values_ );
  ready_ = true;
}//endfunc
//****************************************************************

Graph_ID LocalGraph::CreateNewGraph ( Graph_Init& init, int id )
{
  //InitGraph ( init.map_,init.pos_ );
  my_id.graph_id = id;
  my_id.time_of_creation = ros::Time::now();
  return my_id;
}//endfunc
//****************************************************************

void LocalGraph::SetNextCycleLoopClosing()
{
  close_loop_= true;
}

}//end namespace estimation
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
