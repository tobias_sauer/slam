//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//


#include <gtest/gtest.h>
#include <ros/ros.h>

//messages
#include "d4_comm_pkg/PoseVel.h"
#include "fssim_common/ResState.h"
#include "fssim_common/State.h"
#include "d4_comm_pkg/PoseVel.h"

//algo
#include "main/semaphore.hpp"
#include "vector"



//*************************************************************
//FUNCTIONS Prototypes

static void CBGroundThrouth(const fssim_common::State::ConstPtr &state);
static void CBEstimationResult(const d4_comm_pkg::PoseVel::ConstPtr &state);
static void CBRest(const fssim_common::ResState::ConstPtr &state);
static double ConductTest();
static double GetDifferenceFromSoll(const d4_comm_pkg::PoseVel &state);
bool WithinLimits(const ros::Time &t1,const ros::Time &t2,const ros::Duration  &deviation ) 
{
  if(t1 < t2 + deviation && t1 > t2 -deviation) return true;
    else return false;
}
//*************************************************************
//SYNC
static my_semaphore signal_ready;

//Var
static std::vector<fssim_common::State> vec_soll;
static std::vector<d4_comm_pkg::PoseVel> vec_ist;
static bool received_res = false;

//Subcribers
static ros::Subscriber sub_res;
static ros::Subscriber sub_state_sim;
static ros::Subscriber sub_state_vehicle;



//*************************************************************
//FUNC IMPL
static void CBGroundThrouth(const fssim_common::State::ConstPtr &state)
{
  vec_soll.push_back(*state);
}//End func

static void CBEstimationResult(const d4_comm_pkg::PoseVel::ConstPtr &state)
{
  vec_ist.push_back(*state);
}//End func

static void CBRest(const fssim_common::ResState::ConstPtr &state)
{
  if(state.operator*().emergency == true)
  {
    received_res = true;
    signal_ready.Give();
  }
}//End func

static double ConductTest()
{
  double diff = 0;
  for(const auto &s : vec_ist)
  {
    diff += GetDifferenceFromSoll(s);
  }
  return diff;
  
}//End func

static double GetDifferenceFromSoll(const d4_comm_pkg::PoseVel &state)
{
  ros::Duration d(0.005); //deviation: 5ms
  for(const auto &target : vec_soll)
  {
    if( WithinLimits(state.header.stamp,target.header.stamp,d) == true) //5ms
    {
      return std::hypot(std::abs(state.Pose.pos_x)-std::abs(target.x),std::abs(state.Pose.pos_y)-std::abs(target.y));
    }
  }
  return 0;
}


//*************************************************************
//TEST IMPL
TEST(TestOk1, ReceivedEndSignal){
  EXPECT_TRUE(received_res);
}


TEST(TestOk2, ReceivedSoll){
  EXPECT_GT(vec_soll.size() , 1000);
}


TEST(SkidpadTest2, ReceivedIst){
  EXPECT_GT(vec_ist.size() , 100);
}

TEST(SkidpadTest3, PositionDeviation){
  double result = ConductTest();
  double siz_ist = vec_ist.size();
  result = result/ siz_ist;
  ROS_ERROR_STREAM( "Average Position Deviaton is :" << result << " m ");
  //TEST_COUT << "Average Position Deviaton is :" << result << " m " << std::endl;
  EXPECT_LT(result, 1.0);

}




int main(int argc, char** argv){
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc,argv,"testing");
  ros::NodeHandle nh;
  sub_res = nh.subscribe ( "/fssim/res_state",1,CBRest  );
  sub_state_sim = nh.subscribe ( "/fssim/base_pose_ground_truth",1,CBGroundThrouth  );
  sub_state_vehicle = nh.subscribe ( "/estimation/slam/pos",1,CBEstimationResult  );
  ros::AsyncSpinner spinner(4); // Use 4 threads
  spinner.start();
  signal_ready.Take();
  spinner.stop(); 
  return RUN_ALL_TESTS();
}
