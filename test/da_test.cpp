//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

#include <gtest/gtest.h>
#include <ros/ros.h>

//messages
#include "d4_comm_pkg/PoseVel.h"
#include "fssim_common/ResState.h"
#include "fssim_common/State.h"
#include "d4_comm_pkg/PoseVel.h"

//algo
#include "main/semaphore.hpp"
#include "vector"

//data association
#include "da/nn_da.hpp"
#include "da/Hungarian.hpp"

//poseobject
#include "data/pose_object.hpp"
#include "data/params.hpp"
#include <memory>

//gtsam data
#include <gtsam/geometry/Pose2.h>

void NewNode();


static estimation::ESTIMATION::DA_Params da_params;
std::unique_ptr< estimation::NN_DA> data_asoc;
std::unique_ptr<estimation::Node> node;
static std::map<uint64_t,estimation::Measurement> cone_map;
//TEST IMPL


TEST(DATEST, CheckLateral)
{

  //reset var
  NewNode();
  cone_map.clear();
  data_asoc = std::make_unique<estimation::NN_DA>(da_params);
  //create measurements
  vector<estimation::Measurement> vec_meas;
  estimation::Measurement meas(std::sqrt(2), 0);
  //add to map w/o cov
  cone_map.insert({1, meas}); //map key is "1"
  data_asoc->SetMap(cone_map);
  //add to
  estimation::Measurement meascone(1, 1);
  Matrix2f cov; //set cov
  cov.setZero();
  cov(0, 0) = 0.1;
  cov(1, 1) = 1.0;
  meascone.meas_covar_ = cov;
  vec_meas.push_back(meascone);
  node->AddMeasurements(vec_meas);

  data_asoc->SetMap(cone_map);
  data_asoc->SetMap(cone_map);
  auto results = data_asoc->NNDataAssociation(node.operator*());
  EXPECT_EQ(1, results.at(0).key_meas_);
}

TEST(DATEST, CheckRadial)
{

  //reset var
  NewNode();
  cone_map.clear();
  data_asoc = std::make_unique<estimation::NN_DA>(da_params);
  //create measurements
  vector<estimation::Measurement> vec_meas;
  estimation::Measurement meas(2, 0);
  //add to map w/o cov
  cone_map.insert({1, meas}); //map key is "1"
  data_asoc->SetMap(cone_map);
  //add to
  estimation::Measurement meascone(1.6, 0);
  Matrix2f cov; //set cov
  cov.setZero();
  cov(0, 0) = 0.2;
  cov(1, 1) = 0.01;
  meascone.meas_covar_ = cov;
  vec_meas.push_back(meascone);
  node->AddMeasurements(vec_meas);

  data_asoc->SetMap(cone_map);
  data_asoc->SetMap(cone_map);
  auto results = data_asoc->NNDataAssociation(node.operator*());
  EXPECT_EQ(1, results.at(0).key_meas_);
}

TEST(DATEST, PerfectlyNOTMatchingCone) //tests 1 perfectly matching cone
{ 

 
  //reset var
  cone_map.clear();
  NewNode();
  data_asoc = std::make_unique<estimation::NN_DA>(da_params);

  //create measurements
  estimation::Measurement meas(5, 5);
  //add to map w/o cov
  cone_map.insert({1, meas}); //map key is "1"
  data_asoc->SetMap(cone_map);
  //add to

  vector<estimation::Measurement> vec_meas;
  Matrix2f cov; //set cov
  estimation::Measurement nmeas(1.0, 1.0);
  cov.setZero();
  cov(0, 0) = 1.0;
  cov(1, 1) = 1.0;
  nmeas.meas_covar_ = cov;

  vec_meas.push_back(nmeas);
  node->AddMeasurements(vec_meas);
  
  data_asoc->SetMap(cone_map);
  
  auto results = data_asoc->NNDataAssociation(node.operator*());
  
  EXPECT_NE(1, results.at(0).key_meas_);
}

TEST(DATEST, PerfectlyMatchingCone) //tests 1 perfectly matching cone
{
  
  //reset var
  cone_map.clear();
  NewNode();
  data_asoc = std::make_unique<estimation::NN_DA>(da_params);
  //create measurements
  vector<estimation::Measurement> vec_meas;
  estimation::Measurement meas(1,1);
  //add to map w/o cov
  cone_map.insert({1, meas}); //map key is "1"
  data_asoc->SetMap(cone_map);
  //add to 
  Matrix2f cov; //set cov
  cov.setZero();
  cov(0,0) = 0.1;
  cov(1, 1) = 0.1;
  meas.meas_covar_ = cov;
  vec_meas.push_back(meas);
  node->AddMeasurements(vec_meas);

  data_asoc->SetMap(cone_map);
    
  auto results = data_asoc->NNDataAssociation(node.operator*());
  EXPECT_EQ(1, results.at(0).key_meas_);
}


int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);

  //create node
  gtsam::Pose2 start_pose(0,0,0); 
  estimation::Pose new_pose(start_pose, ros::Time(0), 0);
  std::vector<estimation::Edge> empty;
  estimation::Node new_node(empty, new_pose);
  node = std::make_unique<estimation::Node>(new_node);

  return RUN_ALL_TESTS();
}

void NewNode()
{
  //create node
  gtsam::Pose2 start_pose(0, 0, 0);
  estimation::Pose new_pose(start_pose, ros::Time(0), 0);
  std::vector<estimation::Edge> empty;
  estimation::Node new_node(empty, new_pose);
  node = std::make_unique<estimation::Node>(new_node);
}