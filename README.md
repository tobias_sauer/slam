# Estimation Slam Packet for the Driverless D4!
The SLAM Package serves 2 Main  Purposes:

 - Localisation in a known environment
 - Mapping an unknown environment  and localizing in that 

To archieve that goal, it fuses the Sensor Data consisting of camera data, lidar data and odometry into a graph, which is minimzed by the **GTSAM 4.0** Solver 

# Features

 - High Accuracy at mediocre sensor Data ~0.05m average Deviation
 - Low Latency <10 ms
 - Ability to cope with really bad Sensor Data
 - Automatic Keyframe Creation
 - Easy addition of an almost unlimited amount of Sensors
 - Delay Compensation
 - CI
 - Parameterisable
 - Doxygen Style Documentation
 - Global Nearest Neighbour based on Mahalanobis Distance for Data Association
 - Low computational effort

# TODO

 - Mapping
 - Testing
 - Vehicle Integration
 - Unit Tests for parameter sanity
 - Integration of other solver
 - Integration of other data association
 - Everything else mentioned in Git Issues

# Installation 
The SLAM Packets depends on:  
ROS  
FSSIM  
D4ComPkg  
VehicleComPkg (legacy D3, for driving on the Mockup)  
Eigen  
PCL  
Boost  
GTSAM  

to install GTSAM clone [https://github.com/borglab/gtsam](https://github.com/borglab/gtsam)  
mkdir build && cd build  
cmake -DGTSAM_USE_SYSTEM_EIGEN=ON ..  
sudo make install  

# Configuration

A Simulation Configuration for Skidpad is used as an example  

 estimation/slam/plattform: 1 #Sim = 1, Marina = 2, D4= 3 **based on this results, different data types for input are choosen, so dont get this wrong**  
estimation/slam/mode: 1 #localization = 1, Mapping = 2 **should be clear**  

#general  
#estimation/slam/base_link: "gotthard/base_link"  fssim/vehicle/base_link  
estimation/slam/base_link: "fssim/vehicle/base_link" **Base Link for the vehicle**  
estimation/slam/map_link: "map"    
estimation/slam/wait_for_res: true  **If true, waits until a RES is triggered**  
#frontend  
estimation/slam/frontend_ttl: 15.0 #lifetime for objects in graph, in seconds, should be higher than window size  **Parameter options, good as it is**  
estimation/slam/frontend_max_key_frame_deviation: 0.001 # +- , in sec  **Incoming data which deviates by that amount will be merged into a keyframe. Merging is based on time stamps in header**  
estimation/slam/frontend_linearize_velocity: false # if no external velocity is given  **TODO, only works with fast sensors e.g. Lidar**  

###sensors  
estimation/slam/sensor_count: 2  **dynamic sensor config, number of sensors. Must match the pattern**  
 **Pattern is : estimation/slam/s< sensor number >_....**  
estimation/slam/s1_id: 1  
estimation/slam/s1_name: "Lidar_Front"  
estimation/slam/s1_enable_logging: true **deprecated**  
estimation/slam/s1_type: 1 #lidar 1, camera 2  **deprecated**  
estimation/slam/s1_topic: "/lidar/cones"  
estimation/slam/s1_external_covar_given: false  
estimation/slam/s1_cov_r: 0.2 #in m  **@ 1 sigma**  
estimation/slam/s1_cov_b: 0.02 #in rad **@ 1 sigma**     

estimation/slam/s2_id: 2  
estimation/slam/s2_name: "Mono1"  
estimation/slam/s2_enable_logging: true **deprecated**  
estimation/slam/s2_type: 2 #lidar 1, camera 2 **deprecated**  
estimation/slam/s2_topic: "/camera/cones"  
estimation/slam/s2_external_covar_given: false  
estimation/slam/s2_cov_r: 0.5 # in m **@ 1 sigma**   
estimation/slam/s2_cov_b: 0.02 # in rad **@ 1 sigma**    




###velocity  
estimation/slam/vel_topic: "/fssim/base_pose_ground_truth"  **topic for velocity data, , depends on plattform**  
estimation/slam/vel_external_covar_given: false **TODO**  
estimation/slam/vel_cov_x: 0.5 # # in m  
estimation/slam/vel_cov_y: 0.05 # # in m  
estimation/slam/vel_cov_theta: 0.05 # in rad  

#map input   
estimation/slam/map_topic: "/fssim/track" **track input, depends on plattform**  
estimation/slam/map_external_covar_given: false  
estimation/slam/map_cov_x: 0.05 # in m  
estimation/slam/map_cov_y: 0.05 # in m  



###general  
estimation/slam/start_x: -16.5 **should be clear**  
estimation/slam/start_y: 0.0 **should be clear**  
estimation/slam/start_theta: 0.0 **should be clear**  


#slam  
estimation/slam/localization_mode: 3 # 1= derived, 2 windowed, 3 concurrent windows **leave it at 3, basically a high perfomance mode, the other are debugging and testing**  


##Graph Options  
estimation/slam/graph_con_lifetime: 5.0 #in seconds **just leave it as it ist**  
estimation/slam/graph_window_size: 1 #in seconds  
estimation/slam/graph_compensate_delay: true **compensates the cone delay of meausrments, leave it as it is until posedelaycomp module is in place**  
estimation/slam/graph_add_velocity_to_output: true **same arguement**  


#tf  
estimation/slam/tf_frequency : 50 # in Hz   

#viz
estimation/slam/viz_display_map : true  
estimation/slam/viz_display_temp_map : true  
estimation/slam/viz_display_pose : true  

estimation/slam/viz_map_topic : "estimation/slam/viz_map"  
estimation/slam/viz_temp_map_topic : "estimation/slam/viz_temp_map"  
estimation/slam/viz_pos_topic : "estimation/slam/viz_pos"  


#pub  
estimation/slam/pub_map_topic: "estimation/slam/raw_map"  
estimation/slam/pub_pos_topic: "estimation/slam/pos"  **topic of state output e.g.position and velocity**  

#debug  
estimation/slam/dbg_print_calc_time: true  
estimation/slam/dbg_print_matches: true  
estimation/slam/dbg_print_meas_adeed: true  
estimation/slam/dbg_print_locks: true  


# CI   
Currently the ci consists of 3 Stages:  
1. **Building:** Build the Node + Tests  
2. **Unit Test:** Tests the node against a rosbag on a skidpad and calculates the average deviation (atm 0.05m)  
3. **Integration Test:** Tests the node in FSSIM in a skidpad environment together with control Algorithms  

**Planned**  

 - Rosbags with reel sensor Data
 - Mapping 
 - Autox
 - Integration tests with camera, lidar and velocity estimation
