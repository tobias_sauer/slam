//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/** Graph Init
 * 
 *  @author Tobias Sauer
 *  @date 20.6.2020
 */

#include <ros/ros.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/inference/Key.h>
#include <gtsam/geometry/Pose2.h>
#include "eigen3/Eigen/Core"
#include "map"
#include "data/pose_object.hpp"
namespace estimation{
using namespace std;
/**
 * @brief Conatainer for a graph init
 * 
 */

class Graph_Init
{
public:
  Graph_Init() {};
  std::map<uint64_t,Measurement> map_;
  gtsam::Pose2 pos_;
  Eigen::Matrix3f pos_covar_;
private:
};

/**
 * @brief Container for Graph Id
 * 
 */
class Graph_ID
{
public:
  Graph_ID() {};
  ros::Time time_of_creation;
  ros::Duration max_lookback;
  int graph_id;
};
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
}