//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/** PoseLogger
 * 
 *  @author Tobias Sauer
 *  @date 26.6.2020
 */

#ifndef POSELOGGER_H
#define POSELOGGER_H


#include "ros/ros.h"
#include "ros/package.h"
#include "data/params.hpp"
#include "data/pose_object.hpp"
#include <map>
#include <queue>
#include "main/semaphore.hpp"
#include <thread>
#include <mutex>
#include <iostream>
#include <fstream>
namespace estimation{
/**
 * @brief Logs Poses based on Sensor params into the log folder, dat in SCV, name according to Date
 * 
 */
class PoseLogger
{
public:
 /**
  * @brief Constructor, takes Logger Params to create. Inits the class and opens the file 
  */
 PoseLogger(ESTIMATION::Frontend_Params params);
 /**
  * @brief Log the current poseobect in relation to map data. Logging occours threaded, so this will only add the data to a queue 
  * 
  * @param p object
  * @param map reference map
  */
 void Log(Node &p,std::map<size_t, Measurement> &map );
private:
 std::mutex lock_;
 my_semaphore signal_;
 std::queue<Node> logging_queue_;
 std::map<size_t,Measurement> *map_;
 ESTIMATION::Frontend_Params params_;
 void LoggingTread();
 bool ShallSensorBeLogged(Node &p);
 void LogSensor(Node &p);
 std::string path_;
 bool logging_enabled_ = {false};
};

}

#endif // POSELOGGER_H
