//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/** Params
 * 
 *  @author Tobias Sauer
 *  @date 20.6.2020
 */

#include "ros/ros.h"
namespace estimation{
namespace ESTIMATION
{
namespace PLATTFORM
{
const int SIM = 1;
const int MARINA = 2;
const int D4 = 3;
}

namespace SENSOR_TYPE
{
const int LIDAR = 1;
const int CAMERA = 2;
}

namespace MODE
{
const int LOCALISATION = 1;
const int MAPPING = 2;
}

namespace GRAPHMODE_LOCALIZATION
{
const int STANDARD = 1;
const int WINDOWED = 2;
const int CON_WINDOWS = 3;
}

/**
 * @brief Container for the Sensor Parameters
 * 
 */



class Sensor_Params
{
public:
  Sensor_Params() {};
  int id;
  int type;
  int plattform;
  bool covar_given = {false};
  float covar_r;
  float covar_b;
  ros::NodeHandle *nh_;
  bool logging = {false};
  std::string topic;
  std::string name;
  std::string base_link;
  bool unary_feature_matching{false};
};

/**
 * @brief Contrainer for the Veclocity integrator parameters
 * 
 */
class Velocity_Params
{
public:
  Velocity_Params() {};
  std::string topic_;
  int plattform_;
  ros::NodeHandle *nh_;
  float cov_x;
  float cov_y;
  float cov_thetha;
};

class GPSParams
{
  public:
  GPSParams(){};
  int plattform{1};
  std::string topic;
  float frequency;
  float cov_x;
  float cov_y;
  float cov_theta;
  ros::NodeHandle *nh_;
};

/**
 * @brief Container for Map input parameters
 * 
 */
class Map_Input_Params
{
public:
  Map_Input_Params() {external_covar_given = false;};
  std::string topic_;
  int plattform_;
  ros::NodeHandle *nh_;
  bool external_covar_given;
  float cov_x;
  float cov_y;
};

/**
 * @brief Container for data association parameters
 * 
 */
class DA_Params
{
public:
  DA_Params() {};
  float chi_sq {0.95f};
  int count{5};
  int ttl{10};
};


class Logger_Params
{
public:
  Logger_Params() {};
  std::vector<Sensor_Params> sensors;
};

/**
 * @brief Contains the whole Frontend parameters with all Submodules (DA, Sensor Inputs etc)
 * 
 */
class Frontend_Params
{
public:
  int plattform;
  int mode;
  int number_sensors;
  float start_x;
  float start_y;
  float start_theta;
  float TTL;
  double max_key_frame_deviation;
  bool wait_for_res = {false};
  DA_Params da_params;
  std::vector<Sensor_Params> sensors;
  Velocity_Params velocity_params;
  Map_Input_Params map_input_params_;
  std::string topic_input_map;
  ros::NodeHandle *nh_;
  GPSParams gpsparams_;
  bool gps_activated{false};
  double startup_delay{1.0};
  bool auto_trigger = {true};
  double trigger_frequency = {5.0}; //doesnt work out with real frequency, take factor 3 or so, lol
  bool compensate_delay = {true};
};

/**
 * @brief Contrains Parameters, how the Graph shall behave as well as starting conditions
 * 
 */
class Graph_Params
{
public:
  Graph_Params() {};
  int mode;
  float graph_con_lifetime;
  float start_x;
  float start_y;
  float start_theta;
  int graph_mode_localization;
  int localization_keyframes;
  int localization_new_graph_after;
  int mapping_keyframes;
  int mapping_new_graph_after;
  ros::NodeHandle *nh_;
};

/**
 * @brief contain Parameters for Publishing Topic and Vizuaization to ROS
 * 
 */
class Pub_Params
{
public:
  Pub_Params() {};
  int plattform;
  bool viz_finalised_map;
  bool viz_temporary_map;
  bool viz_pos;
  std::string topic_viz_finalised_map;
  std::string topic_viz_temporary_map;
  std::string topic_viz_pos_map;
  std::string frame_map;
  std::string frame_car;

  std::string topic_position;
  std::string topic_map;
  ros::NodeHandle *nh_;

};

/**
 * @brief Contains the Information for the transformation Publisher
 * 
 */
class TF_Params
{
public:
  TF_Params() {};
  std::string frame_map;
  std::string frame_car;
  float start_x;
  float start_y;
  float start_theta;
  ros::NodeHandle *nh_;
};

/**
 * @brief Container for all params, to be loaded by the param loader
 * 
 */
class Params_S
{
public:
  int mode;
  ros::NodeHandle *nh_;
  Params_S() {};
  int plattform;
  bool window_mode;
  float window_size;
  TF_Params tf_params;
  Pub_Params pub_params;
  Graph_Params graph_params;
  Frontend_Params frontend_params;
  DA_Params da_params;
};
}
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
