//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/** Frontend
 * 
 *  @author Tobias Sauer
 *  @date 20.6.2020
 */

#include "ros/ros.h"
#include "data/params.hpp"
#include <string>
#include <sstream>
#include "global_options.hpp"
namespace estimation{
/**
 * @brief Loads the params from ros 
 * 
 */
class ParamLoader
{
public:
  /**
   * @brief loads the params into the referenced Params object
   * 
   * @param params reference to params
   * @param nh pointer to NodeHandle
   */
  void LoadParams ( ESTIMATION::Params_S &params,ros::NodeHandle *nh );
private:
  void LoadTFParams ( ESTIMATION::TF_Params &params,ros::NodeHandle *nh );
  void LoadPubParams ( ESTIMATION::Pub_Params &params,ros::NodeHandle *nh );
  void LoadFrotendParams ( ESTIMATION::Frontend_Params &params,ros::NodeHandle *nh );
  void LoadGraphParams ( ESTIMATION::Graph_Params &params,ros::NodeHandle *nh );
  void LoadDebugFlags ( ros::NodeHandle *nh );
  void LoadGPSparams(ESTIMATION::Params_S &params, ros::NodeHandle *nh);
      std::string slam_namespace = {"estimation/slam/"};
};

}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
