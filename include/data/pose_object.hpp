//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/** PoseObject
 * 
 *  @author Tobias Sauer
 *  @date 20.6.2020
 */

#include <ros/ros.h>


#include <gtsam/inference/Symbol.h>
#include <gtsam/inference/Key.h>
#include <gtsam/geometry/Pose2.h>
#include "eigen3/Eigen/Core"
#include "data/params.hpp"
#include "d4_comm_pkg/Cone.h"
#include <list>
#include <mutex>
#include <memory>
namespace estimation{
using namespace std;

/**
 * @brief A magic number, to show, values not beeing initialised
 * 
 */
namespace MAGIC
{
const size_t NUMBER = 238523849;
};

/**
 * @brief Conatiner for single measurements of cones
 * 
 */
class Measurement
{
public:
  Measurement ( float x,float y)
  : x_(x), y_(y)
  {
    key_meas_ = MAGIC::NUMBER;
    meas_covar_.setZero();
  };
  Measurement ( const d4_comm_pkg::Cone &c );
  float x_;
   float y_;
  uint8_t color{8};
  gtsam::Key key_meas_;
  Eigen::Matrix2f meas_covar_;
private:
};

class Edge
{
public:
  Edge(gtsam::Key pose_key_pre,gtsam::Key pose_key_suc,ros::Time pred_stamp,gtsam::Pose2 odo,Eigen::Matrix3f covar):
    pose_key_pre_(pose_key_pre),
    pred_stamp_ (pred_stamp),
    pose_key_suc_ (pose_key_suc),
    odo_(odo),
    covar_(covar)
  {
    if (covar_(0, 0) == 0 || covar_(1, 1) == 0 || covar_(2, 2) == 0)
    {
      ROS_ERROR_STREAM("one edge covariance is at least null in its diagonal, 0 covariances are not valid: ");
      std::__throw_invalid_argument("eigen matrix is null on some values in its diagonal , 0 covariances are not valid");
    }
  }
  const gtsam::Key pose_key_pre_;
  const ros::Time pred_stamp_;
  const gtsam::Key pose_key_suc_;
  const gtsam::Pose2 odo_;
  Eigen::Matrix3f covar_;
};

class Pose
{
public:
  Pose(gtsam::Pose2 pose,ros::Time time,gtsam::Key key_pose) : time_(time), key_pose_(key_pose)
  {  pose_=pose; }
  gtsam::Pose2 pose_;
  const ros::Time time_; 
  const gtsam::Key key_pose_;
  Eigen::Matrix3f pos_covar_;
};

using Result = Pose;

/**
 * @brief Conatiner for the Poseobject, the whole Application is centered around this Data. Also contains debugging features
 * 
 */
class Node
{
public:
  Node(const std::vector< Edge> &edges, Pose &pose):
  edges_(edges),
  pose_(pose)
   {
     meas_mut_ = std::make_shared<std::mutex>();
     if(pose.key_pose_ == 0)
     {
       ROS_ERROR_STREAM("no key or key '0' set for Node");
       std::__throw_invalid_argument("no key or key '0' set for Node");
     }
   }

  bool IsDataAsociated() const;
  const std::vector<Edge> edges_;
  Pose pose_;
  ESTIMATION::Sensor_Params sensors_params;
  const std::vector<Measurement> GetMeasuremnts() const;  
  void UpdateMeasurementKeys(const std::vector<Measurement> &meas);  
  void AddMeasurements(const std::vector<Measurement> &meas);
  void SetGpsPose(gtsam::Pose2 gps_pose, Eigen::Matrix3f cov);
  bool IsGps()const;
  Eigen::Matrix3f GetGPSCovar() const;

  size_t GetNumberOfMeasurements() const;
  /**
   * @brief asks if object pose is used by graph id
   * 
   * @param id ...
   * @return bool
   */
  bool IsPoseUsed ( size_t id );
  /**
   * @brief asks if object measurments are used by graph id
   * 
   * @param id ...
   * @return bool
   */
  bool IsMeasUsed ( size_t id );
  /**
   * @brief Set object pose is used by graph id
   * 
   * @param id ...
   * @return void
   */
  void SetPosUsed ( size_t id );
  /**
   * @brief set object measurments are used by graph id
   * 
   * @param id ...
   * @return void
   */
  void SetMeasUsed ( std::size_t id );
  void SetPoses ( vector<pair<float,float>> input );
  void SetPoseId ( size_t id ) ;
  void DEBUG_PRINT_ALL_LANDMARKS() const;
  void DEBUG_PRINT_MATCHED_LANDMARKS() const;
  void DEBUG_PRINT_POS() const;
  void DEBUG_PRINT_ODO() const;
  void DEUBG_PRINT_MEAS_AND_CONTAINING_LM ( map< size_t, Measurement >& local_map_ ) ;
  void DEBUG_PRINT_COV_POS() const;
  private:
    Eigen::Matrix3f gps_cov_;
    bool is_gps_pose_ = {false};
    bool data_associated = {false};
    vector<Measurement> Meas_;
    mutable std::shared_ptr<std::mutex> meas_mut_{nullptr};
    vector<size_t> poses_accesed_by_graph_id;
    vector<size_t> measurments_accesed_by_graph_id;};
 

}//end namespace estimation
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
