//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/** SLAM Main Class
 *
 *  @author Tobias Sauer
 *  @date 20.6.2020
 *
 *
 */
//ros
#include <ros/ros.h>
//Messages
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/ByteMultiArray.h>
#include <geometry_msgs/AccelStamped.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/PoseStamped.h>
#include "fssim_common/Track.h"
#include "d4_comm_pkg/PoseVel.h"
//Threading && std
#include <thread>
#include <mutex>
#include <condition_variable>
#include <chrono>
#include "math.h"
#include "map"
#include <memory>
#include "string"
//data
#include <gtsam/geometry/Pose2.h>
#include <gtsam/inference/Symbol.h>
//customs
#include "cone_listener.hpp"
#include "map_listener.hpp"
#include "vel_integrator.hpp"
#include "pos_publisher.hpp"
#include "slam/local_graph.hpp"
#include "io/visualizer.hpp"
#include "main/frontend.hpp"
#include "data/param_loader.hpp"
#include "data/params.hpp"
#include "io/pos_publisher.hpp"
#include "io/tf_pub.hpp"
namespace estimation{
using namespace std;
/**
* @class SLAM
*
* @brief Main Entry Point for the SLAM node
*
* Main entry Point for the SLAM node. Contains the main running thread and "owns"
* all other Algorythms. Handles Communication between Frotnend and the Graphs via
* a Broker pattern, calls the Param Looader
*/
class SLAM
{
private:
  Params_S params_;
  double distance_travelled_{0};
  gtsam::Pose2 last_pose;
  //Using Smart Pointer for class member for lazy initilization due to later loaded params
  std::unique_ptr<Frontend> frontend_ = {nullptr};
  std::unique_ptr<Visualization> viz_ = {nullptr};
  std::unique_ptr<Pos_Pub> pub_ = {nullptr};
  std::unique_ptr<TF_Pub> tf_pub_ = {nullptr};
  /**
   * @brief Main thread for Slam, will never leave Function
   */
  void MainThread();
  /**
   * @brief Creates an Graph_Init Object from the current and and Position
   * @param map current map
   * @return return a Graph Init Object
   */
  Graph_Init CreateLocalizationInit ( map< uint64_t, Measurement >& map, gtsam::Pose2 start_pose );
  /**
   * @brief Creates an Graph_Init Object from the current and and Position
   * @param map current map
   * @return return a Graph Init Object
   */
  Graph_Init CreateMappingInit();
  //Localization Inits
  /**
  * @brief Inits The class members according to Standard Localization
  */
  void InitStandardLocalization();
  /**
   * @brief Inits The class members according to windows Localization
   */
  void InitWindowedLocalization();
  /**
   * @brief Inits The class members according to Concurrent Localization
   */
  void InitConcurrentLocalization();
  /**
   * @brief Performes standard Loclalization and updates Objects in Frontend
   * @param graph Reference to Graph Object LocalGraph
   * @param id Reference to Graph_ID
   * @param init reference to Graph_Init object
   * @return Vector of changed Poses
   */
  void IncrementalLocalization ( LocalGraph& graph, Graph_ID& id, Graph_Init& init );
  /**
   * @brief Performes Windowed Loclalization WARNING does not upate Graph, or Objects
   * @param graph Reference to Graph Object
   * @param id Reference to Id
   * @param init reference to Graph_Init object
   * @return Vector of changed Poses
   */
  const std::vector< std::shared_ptr<const Node> > WindowedLocalization ( LocalGraph& graph,const Graph_ID& id,const Graph_Init& init );
  /**
   * @brief Executes the Concurrent Graph state Machine
   */
  void ConcurrentLocalization();
  //Actual Graph Objects
  /**
   * @brief Local Graph Object 1 used in all localization Modes
   */
  LocalGraph local_graph_;
  Graph_ID id_local_graph_;
  Graph_Init init_local_graph_;
  /**
   * @brief Local Graph Object, only used in Concurrent Mode
   */
  LocalGraph local_graph_two_;
  Graph_ID id_local_graph_two_;
  Graph_Init init_local_graph_two_;
  /**
   * @brief states for the Concurrent state machine
   */
  int concurrent_state_;
void Localization();
void HandleOutput();
void ConcurrentWait( LocalGraph &run_graph,LocalGraph &next_graph,Graph_ID &run_id,Graph_ID &next_id,Graph_Init &init);  
void ConcurrentRun ( LocalGraph &run_graph,LocalGraph &next_graph,Graph_ID &run_id,Graph_ID &next_id,Graph_Init &init);
void ConcurrentInit ( LocalGraph &run_graph,Graph_ID &run_id,Graph_Init &init );
void LocalizationInit();
void MappingInit();
void MainLoop();
void Mapping();
void CloseLoop(const d4_comm_pkg::PoseVel &state);
bool IsLoopClosed(const d4_comm_pkg::PoseVel &state);
Node CreateInitNode(gtsam::Pose2 pose,size_t key);
int skip_mapping_{0};
public:
  SLAM();
  ~SLAM() {};
}; //endclass

}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
