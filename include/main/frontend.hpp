//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once

//ros & messages
#include "ros/ros.h"
#include "d4_comm_pkg/ConeArray.h"
#include "d4_comm_pkg/VehicleStateDT.h"
//In, Out, Visualization
#include "io/cone_listener.hpp"
#include "io/map_listener.hpp"
#include "io/vel_integrator.hpp"
#include "io/res.hpp"
#include "io/matching_viz.hpp"
#include "io/gps.hpp"
//Data
#include "data/poselogger.hpp"
#include "data/params.hpp"
#include "data/graph_init.hpp"
#include "data/pose_object.hpp"
#include "main/semaphore.hpp"
#include <gtsam/geometry/Pose2.h>
#include <gtsam/inference/Symbol.h>
#include <logical_graph.hpp>
//Data Association
#include "da/nn_da.hpp"
//STD && alg
#include <functional>
#include <memory>
#include <map>
#include <mutex>
#include "main/semaphore.hpp"
#include <unordered_map> 
//Flags
#include "data/global_options.hpp"
namespace estimation{
/**
* @class Frontend
*
* @brief Handles input and provdes Data to the Backend according to Frontend
* Backend annotation
*
* Handles Optical Sensor Inputs, Velocity, Map. Associates Data with Data assocation
*/
class Frontend
{
public:
  Frontend ( ESTIMATION::Frontend_Params &params );
  std::map<uint64_t,Measurement> GetMap();
  std::map<uint64_t, Measurement> GetNewLandmarks();
  void UpdateMap(std::map<uint64_t, Measurement> map);
  d4_comm_pkg::PoseVel GetActualState();
  void GetTrigger();
  const std::vector<std::shared_ptr<const Node>> GetWindows(double window_size, const Graph_ID &id);
  const std::vector<std::shared_ptr<const Node>> GetNewNodesAndEdges(const Graph_ID &id);
  const std::vector<std::shared_ptr<const Node>> GetMeasurements(const Graph_ID &id);
  void UpdateNodes(const std::vector< Result> &results); 
  void SetLocalizationMode();
private:
  //Attributes:
  std::map<uint64_t,Measurement> map_;
  ros::Timer timer_;
  void CbTimer(const ros::TimerEvent &e);

  //member classes as smart pointer for lazy init
  std::unique_ptr<MapListener> MapListener_ ={nullptr};
  std::shared_ptr<VelocityIntegrator>  vel_integrator_={nullptr}; //shared with other classes
  std::unique_ptr<NN_DA> data_asoc_={nullptr}; 
  std::unique_ptr<PoseLogger> plogger_ ={nullptr};
  std::unique_ptr<RESLister> res_listener_ = {nullptr};
  std::unique_ptr<LogicalGraph> logical_graph_{nullptr};
  std::unique_ptr<MatchingViz> matching_viz_{nullptr};
  std::unique_ptr<GPS> gps_node_{nullptr};
  vector<std::unique_ptr<Cone_Listener>> cone_inputs_;
  my_semaphore signal_;
  ESTIMATION::Frontend_Params params_;
  my_semaphore signal_slam_;
  //Methods
  void CBMap (const d4_comm_pkg::RawMap& map );
  void CBRes();
  void CBCones(d4_comm_pkg::ConeArray cones, int sensor_id);
  void AddProcessMeasurements(std::shared_ptr<Node> &p, const d4_comm_pkg::ConeArray &cones);
  ESTIMATION::Sensor_Params GetParamsFromID(int sensor_id) const;
  typedef struct
  {
    bool received_res_ = {false};
    bool received_map_ = {false};
    bool initalized_frontend = {false};
    bool timer = {false};
  }Init_States;
  Init_States init_states_;
  bool Initialized() const;
  void EvaluateTrigger(int sensor_id);
  void CbGPS(gtsam::Pose2 gps_pose, ros::Time stamp, Eigen::Matrix3f cov);
};
}//end namespace estimation
