//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once

//ros & messages
#include "ros/ros.h"
#include "d4_comm_pkg/ConeArray.h"
#include "d4_comm_pkg/VehicleStateDT.h"
//In, Out, Visualization
#include "io/cone_listener.hpp"
#include "io/map_listener.hpp"
#include "io/vel_integrator.hpp"
#include "io/res.hpp"

//Data
#include "data/poselogger.hpp"
#include "data/params.hpp"
#include "data/graph_init.hpp"
#include "data/pose_object.hpp"
#include "main/semaphore.hpp"
#include <gtsam/geometry/Pose2.h>
#include <gtsam/inference/Symbol.h>

//STD && alg
#include <functional>
#include <memory>
#include <map>
#include <mutex>
#include "main/semaphore.hpp"
#include <unordered_map>
//Flags
#include "data/global_options.hpp"
namespace estimation
{

  class LogicalGraph
  {
  public:
    LogicalGraph(std::shared_ptr<VelocityIntegrator> vel_integrator, ESTIMATION::Frontend_Params &params);
    std::shared_ptr<Node> AddMeasurementToGraph(const ros::Time &stamp);
    const std::vector<std::shared_ptr<const Node>> GetWindow(double window_size, const Graph_ID &id);
    std::shared_ptr<Node> GetLatestNode();
    const std::vector<std::shared_ptr<const Node>> GetNewNodesAndEdges(const Graph_ID &id);
    const std::vector<std::shared_ptr<const Node>> GetMeasurements(const Graph_ID &id);
    void UpdateNodes(const std::vector<Result> &results);
    std::shared_ptr<Node> GetNodeByID(size_t id);
  private:
    //Atributes
    std::shared_ptr<Node> latest_pose_object_;
    std::shared_ptr<VelocityIntegrator> vel_integrator_;
    gtsam::Key pose_keys_;
    std::list<std::shared_ptr<Node>> pose_object_container_;
    std::unordered_map<size_t, std::shared_ptr<Node>> pose_object_map;
    ESTIMATION::Frontend_Params params_;
    std::mutex  graph_lock_;
    Eigen::Matrix3f covar_odo_;
    //****************************************************************
    //Methods
    gtsam::Key GetNextKey();
    std::shared_ptr<Node> CreateReturnLatestPoseObject(const ros::Time &stamp);
    std::shared_ptr<Node> CreateFirstPoseObject(const ros::Time &stamp);
    Node CreateLatestPoseObject(const ros::Time &stamp);
    void EvaluateNeighbours(const ros::Time &stamp, std::shared_ptr<Node> &predecessor, std::shared_ptr<Node> &sucessor);
    std::shared_ptr<Node> FindKeyPose(const ros::Time &stamp);
    std::shared_ptr<Node> CreateReturnInbetweenNode(const ros::Time &stamp);
    std::shared_ptr<Node> CreateInbetweenNode(const ros::Time &stamp, const std::shared_ptr<const Node> &predecessor, const std::shared_ptr<const Node> &sucessor);
    shared_ptr<Node> FindCreateMatchingPose(const ros::Time &stamp);
    std::shared_ptr<Node> CreateNewFirstNodeWithSucessor(const ros::Time &stamp, const std::shared_ptr<const Node> &sucessor);
    void CleanUpGraph();
    void UpdateFromLasttoSource(const std::shared_ptr<Node> &source);
    void SortObjectContainerAscendingTimestamps();
    void CheckGraph();
    //****************************************************************
    //helper
    bool WithinLimits(const ros::Time &t1, const ros::Time &t2, const ros::Duration &deviation) const;
  };

} //end namespace estimation