//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once
#include "thread"
#include "mutex"
#include "condition_variable"
/**
 * @brief Fast implementation for std semaphores 
 * 
 */
class my_semaphore
{
private:
  std::mutex m;
  std::condition_variable cv;
  bool ready;
public:
  my_semaphore()
  {
    ready=false;
  };
  /**
   * @brief Blocks until semaphore is given, will set it to taken if operation sucessfull
   * 
   * @return void
   */
  void Take()
  {
    std::unique_lock<std::mutex> lk(m);
    while (ready == false)
    {
      cv.wait(lk);
    }
    ready = false;
  }
  /**
   * @brief Lets objects, which are blocked on take proceed
   * 
   * @return void
   */
  void Give()
  {
    std::lock_guard<std::mutex> lk(m);
    ready = true;
    cv.notify_one();
  }

  void Reset()
  {
    ready=false;
  }
};
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
