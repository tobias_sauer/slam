//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//

/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once



#include <ros/ros.h>
// GTSAM headers
// Both relative poses and recovered trajectory poses will be stored as Pose2 objects
#include <gtsam/geometry/Pose2.h>

// Each variable in the system (poses and landmarks) must be identified with a unique key.
// We can either use simple integer keys (1, 2, 3, ...) or symbols (X1, X2, L1).
// Here we will use Symbols
#include <gtsam/inference/Symbol.h>
#include <gtsam/inference/Key.h>

// We want to use iSAM2 to solve the range-SLAM problem incrementally
#include <gtsam/nonlinear/ISAM2.h>

// iSAM2 requires as input a set set of new factors to be added stored in a factor graph,
// and initial guesses for any new variables used in the added factors
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/Values.h>

// We will use a non-liear solver to batch-inituialize from the first 150 frames
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>

// In GTSAM, measurement functions are represented as 'factors'. Several common factors
// have been provided with the library for solving robotics SLAM problems.
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/sam/RangeFactor.h>
#include <gtsam/sam/BearingRangeFactor.h>
#include <gtsam/slam/dataset.h>


#include "Eigen/Core"
#include "Eigen/Eigen"

#include "data/graph_init.hpp"
#include "data/pose_object.hpp"

#include <thread>
namespace estimation{
  using namespace std;

  /**
  * @class LocalGraph
  * @brief Wraps the ISAM2 Algorithms in this class and makes is accesable via the central Pose Object.
  * Also provides a set of operations as stated in the Documentation
  */
  class LocalGraph
  {
  public:
      LocalGraph() {};
      map< uint64_t, Measurement>  GetMap();
      Graph_ID CreateNewGraph ( Graph_Init& init,int id );
      void Update();
      const std::vector< Result> UpdatePoseObjects (const std::vector< std::shared_ptr<const Node> >&nodes );
      const std::vector< Result> UpdatePoseObjects(const std::vector< std::shared_ptr<const Node> > &nodes
                                      ,const std::vector< std::shared_ptr<const Node> > &measurements );

      void AddMeasurements ( const std::vector<std::shared_ptr<const Node> >  &nodes );
      void AddPoses (const std::vector<std::shared_ptr<const Node> > &nodes );
      Eigen::Matrix3f GetLastCovar();
      Eigen::Matrix3f GetPoseCovar ( Node &node );
      gtsam::Pose2 GetUpdatedPose();
      void AddNewLandmarks(map<uint64_t, Measurement> input);
      /**
       * @brief Init a new Graph with its Poses, edges and Measurements and Map
       */
      void AddInitWindow (const std::vector<std::shared_ptr<const Node> > &nodes,const Graph_Init& init,const Graph_ID &id );
      void ComputeUpdateThreaded ( bool detached );
      bool ready_;
      void ClearValues();
      void ResetGraph();
      void SetNextCycleLoopClosing();
      void InitGraph(const map<uint64_t, Measurement> &input, const Node &node);
    private : 
      void AddPoseToGraph (const Node &o );
      void AddMeasurementsToGraph (const Node &o );
      void AddMapToGraph (const map< uint64_t, Measurement> &input );
      void ComputationTread();
      
      Graph_Init GetNewGrapInit();
      Graph_ID my_id;
      gtsam::ISAM2 isam;
      gtsam::Pose2 last_pose;
      gtsam::Values local_values_;
      gtsam::NonlinearFactorGraph local_graph_;
      gtsam::Key last_pose_key_;
      bool close_loop_{false};
      bool first_key_received{false};
      size_t first_key;
      map< uint64_t, Measurement > local_map_;
      bool initalised_{false};
      std::set<size_t> added_poses;
  };
}//end namespace estimation
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
