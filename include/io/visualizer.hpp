//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */


#include "ros/ros.h"
#include "d4_comm_pkg/PoseVel.h"
#include "d4_comm_pkg/SdcOut.h"
#include <visualization_msgs/Marker.h>
#include "map"
using namespace std;

class Visualization
{
public:
  Visualization ( ros::NodeHandle* nh );
  void PubMap ( map< uint64_t, pair< float, float > >& input );
  void PubCone ( float x, float y );
  void Relative ( vector< pair< float, float > >& cones );
  void VizPath ( float x, float y,bool derived );
private:
  uint32_t seq_;
  uint32_t seq_cones_;
  ros::Publisher pub_map_;
  ros::Publisher pub_cone_;
  ros::Publisher pub_relative_;
  ros::Publisher pub_path_;


};
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
