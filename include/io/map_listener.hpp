//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once
#include "ros/ros.h"
#include "fssim_common/Track.h"
#include "d4_comm_pkg/RawMap.h"
#include "data/params.hpp"
#include "main/semaphore.hpp"
#include "functional"

namespace estimation{
class MapListener
{
private:

  void CBFSMap ( const fssim_common::Track::ConstPtr& Track );
  void CBD4Map ( const d4_comm_pkg::RawMap::ConstPtr& Track );
  void AddToMapFromFSSIM(d4_comm_pkg::RawMap &map,const std::vector<geometry_msgs::Point> &source);
  d4_comm_pkg::RawMap map_;
  std::function<void ( d4_comm_pkg::RawMap & ) > cb_;
  ros::Subscriber sub_;
  my_semaphore signal_;
  ESTIMATION::Map_Input_Params params_;

public:

  MapListener ( ESTIMATION::Map_Input_Params &params,std::function<void ( d4_comm_pkg::RawMap & ) > cb );
  d4_comm_pkg::RawMap Get_Map();
};
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
