//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */

#include "ros/ros.h"
#include <visualization_msgs/Marker.h>
#include "map"
#include "queue"

#include "pose_object.hpp"
#include <thread>
#include "main/semaphore.hpp"
#include "util/blockingconcurrentqueue.h"
namespace estimation{
class MatchingViz
{
public:
  MatchingViz(ros::NodeHandle *nh);
  void VizualizeMatching(std::shared_ptr<Node> node, const std::map<uint64_t, Measurement> &map);
  std::thread pub_t;

private:
  util::BlockingConcurrentQueue<std::pair<std::shared_ptr<Node>, std::map<uint64_t, Measurement>>> queue;
  std::map<uint64_t, Measurement> map_;
  std::thread t1;
  void VizThread();
  std::queue<Node> processing_queue_;
  std::mutex lock_queue;
  my_semaphore signal_;
  uint32_t seq_;
  ros::Publisher pub_viz_;
};
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on;
