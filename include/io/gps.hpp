//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once
//Messages && ros
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include "d4_comm_pkg/ConeArray.h"
#include "data/params.hpp"
#include <gtsam/geometry/Pose2.h>
#include "fssim_common/State.h"
//STD && Algo
#include "string.h"
#include "queue"
#include "functional"
#include "random"
namespace estimation
{
  class GPS
  {
    public:
      GPS(const ESTIMATION::GPSParams &params, std::function<void(gtsam::Pose2, ros::Time, Eigen::Matrix3f)> callback);

    private:
      ros::Time last_sampe_time_{0};
      ESTIMATION::GPSParams params_;
      void CallbackSim(const fssim_common::State_<std::allocator<void>>::ConstPtr &speed_in);
      void CallbackMarina();
      void CallbackD4();
      std::function<void(gtsam::Pose2,ros::Time, Eigen::Matrix3f)> cb_data;
      ros::Subscriber sub_gps_;
  };//end class
}//end namespace