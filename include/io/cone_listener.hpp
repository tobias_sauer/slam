//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once
//Messages && ros
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include "d4_comm_pkg/ConeArray.h"
#include "data/params.hpp"
//PCL
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/common/common.h>
//STD && Algo
#include "semaphore.hpp"
#include "string.h"
#include "queue"
#include <sensor_msgs/point_cloud2_iterator.h>
#include "functional"
//TF
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>
#include <memory>
#include "geometry_msgs/PointStamped.h"

#include "tf2_ros/message_filter.h"
#include "message_filters/subscriber.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
namespace estimation{
using namespace std;
class Cone_Listener
{
private:
  ESTIMATION::Sensor_Params params_;
  std::function<void ( d4_comm_pkg::ConeArray,int ) > cb_data;
  ros::Subscriber sub_cone_;
  d4_comm_pkg::ConeArray PCL_to_cone_array ( const sensor_msgs::PointCloud2::ConstPtr &cones_in );
  void TransformToBaseLink(d4_comm_pkg::ConeArray &cones);
  void CBCones ( const d4_comm_pkg::ConeArray::ConstPtr &cones_in );
  void CBPCL ( const sensor_msgs::PointCloud2::ConstPtr &cones_in );
  bool base_link_initialized = {false};
  geometry_msgs::TransformStamped transform_stamped_;
  void InitBaseLinkTransformation(d4_comm_pkg::ConeArray &cones);
  void AddMissingInformation(d4_comm_pkg::ConeArray &cones);
  tf2_ros::Buffer tfbuffer_;
  std::unique_ptr<tf2_ros::TransformListener> tfListener_{nullptr};

public:
  Cone_Listener ( ESTIMATION::Sensor_Params& params, std::function< void ( d4_comm_pkg::ConeArray,int ) > callback );
};

}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
