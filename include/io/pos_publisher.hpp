//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */
#pragma once
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "d4_comm_pkg/PoseVel.h"
#include "geometry_msgs/PoseStamped.h"
#include "d4_comm_pkg/RawMap.h"
#include "data/params.hpp"
#include <tf/transform_broadcaster.h>
#include <visualization_msgs/Marker.h>
#include "data/pose_object.hpp"
namespace estimation{
using namespace ESTIMATION;
class Pos_Pub
{
private:
  ros::Publisher pub_rtse_pose;
  ros::Publisher pos_visualize;
  ros::Publisher pub_map;
  ros::Publisher viz_map;
  uint32_t  seq;
  uint32_t  seq_pos_{0};
  ESTIMATION::Pub_Params params_;
public:
  void Publish (  d4_comm_pkg::PoseVel &out  );
  void PublishMap (const std::map< size_t , Measurement>& out, bool finalised );
  Pos_Pub ( ESTIMATION::Pub_Params& params );
  ~Pos_Pub();

};

}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
