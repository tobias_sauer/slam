//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once

/** TF Pub
 * 
 *  @author Tobias Sauer
 *  @date 20.6.2020
 */
#include "ros/ros.h"
#include "data/params.hpp"
#include <tf/transform_broadcaster.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <gtsam/geometry/Pose2.h>
#include <thread>
#include <chrono>
/**
 * @brief Publishes Periodically a TF Transformation matrix
 * 
 */
namespace estimation{

class TF_Pub
{
public:
  TF_Pub ( ESTIMATION::TF_Params &params );
  /**
   * @brief Set the current position of the car. It will then be published Periodically
   * 
   * @param cur_pose Current position
   */
  void Update_TF ( gtsam::Pose2 cur_pose );
private:
  void TF_Thread();
  ESTIMATION::TF_Params params_;
  tf::TransformBroadcaster br_map;
  tf::Transform transform_map;

  tf::TransformBroadcaster br_car;
  tf::Transform transform_car;

  gtsam::Pose2 cur_pose_;
};

}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
