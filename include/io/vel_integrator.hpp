//----------------------------------------------------------------------------//
//                                                                            //
// This software is distributed under the MIT License (MIT).                  //
//                                                                            //
// Copyright (c) 2020 Tobias Sauer                                            //
//                                                                            //
// Permission is hereby granted, free of charge, to any person obtaining a    //
// copy of this software and associated documentation files (the "Software"), //
// to deal in the Software without restriction, including without limitation  //
// the rights to use, copy, modify, merge, publish, distribute, sublicense,   //
// and/or sell copies of the Software, and to permit persons to whom the      //
// Software is furnished to do so, subject to the following conditions:       //
//                                                                            //
// The above copyright notice and this permission notice shall be included in //
// all copies or substantial portions of the Software.                        //
//                                                                            //
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR //
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   //
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    //
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER //
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    //
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        //
// DEALINGS IN THE SOFTWARE.                                                  //
//                                                                            //
//----------------------------------------------------------------------------//
#pragma once
/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 */

#include <ros/ros.h>
// GTSAM headers
// Both relative poses and recovered trajectory poses will be stored as Pose2 objects
#include <gtsam/geometry/Pose2.h>
#include "fssim_common/State.h"
#include "d4_comm_pkg/PoseVel.h"
#include "vehicle_communication_pkg/VehicleState.h"
#include "main/semaphore.hpp"
#include "data/params.hpp"
#include <deque>
namespace estimation{
/**
 * @brief Handles Vel input and integrates them. Can be querried for speed and odometry
 * 
 */

class VelDataPoint
{
  public:
    VelDataPoint(){};
    VelDataPoint(ros::Time ti){t = ti;}
    double vx;
    double vy;
    double vtheta;
    ros::Time t;

};

class VelocityIntegrator
{
private:



  ros::Time last_stamp;
  double v_x;
  std::deque<VelDataPoint> data_points_;
  gtsam::Pose2 actual_pose;
  gtsam::Pose2 retpose_;
  gtsam::Pose2 from_to ( ros::Time begin, ros::Time end );

  double integrated_since_last_;

  ros::Subscriber sub_speed;

  void CallbackSpeedSim ( const fssim_common::State_< std::allocator< void > >::ConstPtr& speed_in );
  void CallbackSpeedD4 ( const d4_comm_pkg::VehicleStateDT::ConstPtr& speed_in);
  void CallbackSpeedVehicleCom ( const vehicle_communication_pkg::VehicleState::ConstPtr& speed_in);
  void AddOdometry ( double x, double y, double phi, ros::Time stamp );
  ESTIMATION::Velocity_Params params_;
  std::mutex mut;
public:
  VelocityIntegrator ( ESTIMATION::Velocity_Params &params );

  /**
   * @brief returns the odometry in the specified time fram
   * 
   * @param begin begin of timeframe
   * @param end end of timeframe
   * @return gtsam::Pose2 odo in timeframe
   */
  gtsam::Pose2 OdoFromTo ( ros::Time begin, ros::Time end );
  gtsam::Pose2 OdoTillNow ( ros::Time begin );
  /**
   * @brief returns current speed
   * 
   * @return d4_comm_pkg::PoseVel
   */
  d4_comm_pkg::PoseVel GetSpeed();
  float GetRatio(const ros::Time t1, const ros::Time t2, const ros::Duration &tstep) const;
  float WeightedValue(float val1, float val2, const ros::Time &t1, const ros::Time &t2, const ros::Duration &tstep) const;
  size_t GetMilsecFromDiff(const ros::Duration &diff)const;
  VelDataPoint GetWeightedObject(const VelDataPoint &a, const VelDataPoint &b, const ros::Duration &tstep)const;

  std::deque<VelDataPoint>::const_iterator GetLowerBound(const std::deque<VelDataPoint> &c, const ros::Time &t)const;
  std::deque<VelDataPoint>::const_iterator GetUpperBound(const std::deque<VelDataPoint> &c, const ros::Time &t) const;
  std::vector<VelDataPoint>::const_iterator GetLowerBound(const std::vector<VelDataPoint> &c, const ros::Time &t) const;
  std::vector<VelDataPoint>::const_iterator GetUpperBound(const std::vector<VelDataPoint> &c, const ros::Time &t) const;

  Eigen::Matrix3f
  GetCov();
};
}
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
