#pragma once

#ifndef ESTIMATION_HUNGARIAN_H
#define ESTIMATION_HUNGARIAN_H

#include <iostream>
#include <Eigen/Dense>
#include <vector>

using namespace std;
using namespace Eigen;

class HungarianAlgorithm
{
public:
  HungarianAlgorithm();
  ~HungarianAlgorithm();
  vector<int> Solve ( vector <vector<float> >& DistMatrix, std::vector<int>& Assignment );

private:
  void assignmentoptimal ( int *assignment, double *cost, double *distMatrix, int nOfRows, int nOfColumns );
  void buildassignmentvector ( int *assignment, bool *starMatrix, int nOfRows, int nOfColumns );
  void computeassignmentcost ( int *assignment, double *cost, double *distMatrix, int nOfRows );
  void step2a ( int *assignment, double *distMatrix, bool *starMatrix, bool *newStarMatrix, bool *primeMatrix, bool *coveredColumns, bool *coveredRows, int nOfRows, int nOfColumns, int minDim );
  void step2b ( int *assignment, double *distMatrix, bool *starMatrix, bool *newStarMatrix, bool *primeMatrix, bool *coveredColumns, bool *coveredRows, int nOfRows, int nOfColumns, int minDim );
  void step3 ( int *assignment, double *distMatrix, bool *starMatrix, bool *newStarMatrix, bool *primeMatrix, bool *coveredColumns, bool *coveredRows, int nOfRows, int nOfColumns, int minDim );
  void step4 ( int *assignment, double *distMatrix, bool *starMatrix, bool *newStarMatrix, bool *primeMatrix, bool *coveredColumns, bool *coveredRows, int nOfRows, int nOfColumns, int minDim, int row, int col );
  void step5 ( int *assignment, double *distMatrix, bool *starMatrix, bool *newStarMatrix, bool *primeMatrix, bool *coveredColumns, bool *coveredRows, int nOfRows, int nOfColumns, int minDim );
};

#endif //ESTIMATION_HUNGARIAN_H


// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
