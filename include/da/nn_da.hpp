/**
 *  @author Tobias Sauer
 *  @email tobias.sauer@greenteam-stuttgart.de
 *  @brief contains da class
 */

#pragma once
//ros
#include "ros/ros.h"
#include <gtsam/geometry/Pose2.h>
//#include <boost/math/distributions/chi_squared.hpp>
#include <boost/math/distributions/non_central_chi_squared.hpp>
#include "Hungarian.hpp"
//io
#include "io/visualizer.hpp"
//data
#include "data/pose_object.hpp"
#include "data/params.hpp"
//std && algo
#include "Eigen/Core"
#include "Eigen/Eigen"
#include <vector>
#include "list"
#include "map"
#include <mutex>
//flags
#include "global_options.hpp"
namespace estimation{
using namespace Eigen;

class Candidate
{
  public:
  Candidate(const Measurement &measu): meas(measu){};
  Measurement meas;
  std::vector<int> found_by_;
  size_t times_found{0};
  size_t TTL{10};
};

class NN_DA
{
private:
  double chi_square;
  gtsam::Pose2 actual_pose;
  std::map<uint64_t,Measurement> local_map_;
  std::map<uint64_t,Measurement> ret_candidates_;
  size_t next_map_key{0};
  std::map< uint64_t,  Measurement > GateMap(const Node& obj,const std::map< uint64_t,  Measurement >& map, float radius)const;
  

  /**
   * @brief Applies MD to d2 Matrix based on map and Poseobject Data
   * 
   * @param d2_matrix ...
   * @param obj ...
   */
  void MahalabonisDistance(vector<vector<float>> &d2_matrix, const Node &obj, const std::map<uint64_t, Measurement> &map, const std::vector<Measurement> &measurements)const;

 
  
  std::vector<Candidate> candidates_;
  
  //void SLAM_Da ( PoseObject &obj );
  std::vector<Measurement> Normal_Da(const Node &obj, const map<uint64_t, Measurement> &map)const;
  ESTIMATION::DA_Params params_;
  Matrix2f TransformEulertoEuclidianCov(const Matrix2f &eul_cov,float range,float bearing)const;
  Vector2f TransformMeasurementToWorldCoordinates(float p_x, float p_y, float p_theta, float t_x, float t_y) const;
  float CalcluateMD(const Vector2f &diff_vec,const Matrix2f covar)const;
  float UnaryFeatureMatching(bool enabled,float d_2,int source_color, int target_color )const;
  void AddCandidatesToMap(map<uint64_t, Measurement> &map);
  std::vector<Measurement> GetNotMachtedResults(const Node &obj, const std::vector<Measurement> &results) const;
  void AddNotMatchedResultsToCandidates(std::vector<Measurement> &results, const Node &n);
  std::vector<Measurement> GetResultsWithoutCanidates(const std::vector<Measurement> &results) const;
  std::vector<Measurement> ExtractCandidatesFromResults(const std::vector<Measurement> &results) const;
  void ProcessCandidates(const Node &obj,const std::vector<Measurement> &results);
  void AddCandidateToMap(const Node &obj, Candidate &c, const Measurement &m);
  Vector2f TransformMeasurementToWorldCoordinates(const gtsam::Pose2 &pose, const Measurement &m)const ;
      size_t canidate_keys{0};
  std::mutex map_mutex_;

public:
  /**
   * @brief Associates Data --> Sets matching Object keys in Poseobjects Measurements
   * 
   * @param obj target Object
   * @param slam UNUSED
   * @return void
   */
  std::vector<Measurement> NNDataAssociation(const Node &obj) const;
  std::vector<Measurement> MappingDataAssociation(const Node &obj);

  void SetMap (const std::map<uint64_t,Measurement> &map_in );
  void Update_Map (const std::map<uint64_t,Measurement> &map_in );
  std::map<uint64_t, Measurement> GetNewLandmarks();
  Visualization *viz_;
//constructor
  NN_DA ( ESTIMATION::DA_Params& params );
};

}//end namespace
// kate: indent-mode cstyle; indent-width 2; replace-tabs on; 
